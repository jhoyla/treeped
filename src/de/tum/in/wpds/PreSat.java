/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// PreSat -- a rewrite of jmoped's jwpds PdsSat class to perform prestar (not
// just post)
//
// (Should just be an extension of PdsSat, but the use of private in that class
// makes it impossible.)
//
// Author: Matthew Hague


package de.tum.in.wpds;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.VarManager;

import de.tum.in.wpds.CancelMonitor;
import de.tum.in.wpds.Config;
import de.tum.in.wpds.Fa;
import de.tum.in.wpds.LifoWorkSet;
import de.tum.in.wpds.Pds;
import de.tum.in.wpds.Rule;
import de.tum.in.wpds.Semiring;
import de.tum.in.wpds.Transition;
import de.tum.in.wpds.WorkSet;



public class PreSat {
    static Logger logger = Logger.getLogger(PreSat.class);

    /**
     * The result of presat -- call free on it when done
     */
    public class PreSatResult {
        /**
         * the fa built
         */
        public Fa fa;
        /**
         * A trace builder if setGenerateTraceBuilder(true) was called.
         */
        public TraceBuilder traceBuilder;

        public PreSatResult(Fa fa, TraceBuilder traceBuilder) {
            this.fa = fa;
            this.traceBuilder = traceBuilder;
        }

        public void free() {
            fa.free();
        }
    }

    private class NewRuleJustification {
        public Rule r;
        public Transition t1;

        /**
         * @param r the rule leading to the addition of a new rule
         * @param t1 the transition leading to the addition of a new rule
         */
        public NewRuleJustification(Rule r, Transition t1) {
            this.r = r;
            this.t1 = t1;
        }
    }

    private boolean generateTraceBuilder = false;
    private TraceBuilder traceBuilder = null;
    private CancelMonitor monitor;
	private Pds pds;
	private PreSatResult sat;
	private WorkSet<Transition> workset = new LifoWorkSet<Transition>();
    private Map<Config,Set<Rule>> rules = new HashMap<Config,Set<Rule>>();
    private Set<Rule> popRules = new HashSet<Rule>();
    private VarManager manager;
    private Map<Rule, NewRuleJustification> newRuleJustifications
        = new HashMap<Rule, NewRuleJustification>();

	public PreSat(Pds pds, VarManager manager) {
		this.pds = pds;
        this.manager = manager;
	}


    /**
     * @param generate true if you want a trace builder returned from the
     * subsequent calls to presat
     */
    public void setGenerateTraceBuilder(boolean generate) {
        generateTraceBuilder = generate;
        if (generate && traceBuilder == null)
            traceBuilder = new TraceBuilder(manager);
    }

    /**
     * @return true if generating trace builders
     */
    public boolean getGenerateTraceBuilder() {
        return generateTraceBuilder;
    }

    /**
     * Build a map of (p,a) -> rules of the form q b --> p a ...
     * clears old map
     */
    private void getPdsRules() {
        rules = new HashMap<Config,Set<Rule>>();
        popRules = new HashSet<Rule>();
        for (Rule r : pds.rules)
            addPdsRule(r);
    }

    private void addPdsRule(Rule r) {
        if (r.right.w.length == 0) {
            popRules.add(r);
        } else {
            Config rhs = new Config(r.right.p, r.right.w[0]);
            Set<Rule> rs = rules.get(rhs);
            if (rs == null) {
                rs = new HashSet<Rule>();
                rules.put(rhs, rs);
            }
            rs.add(r);
        }
    }

    private Set<Rule> getPdsRules(String p, String a) {
        return rules.get(new Config(p, a));
    }

    private Set<Rule> getPopRules() {
        return popRules;
    }


	/**
	 * Updates the saturating automaton with the transition <code>t</code>
	 * and the semiring value <code>d</code>.
	 * The method should add more elements to the worklist
	 * if the transition is new or the semiring value is not already included.
	 *
	 * @param d the sermiring value.
	 * @param t the transition.
     * @param r the rule justifying the transition,
     * @param t1 the first transition justifying the new one (or null)
     * @param t2 the second transition justifying the new one (or null)
	 * @return <code>true</code> iff the transition is new or the semiring
	 *			value is not already included.
	 */
	private boolean update(BDDSemiring d,
                           Transition t,
                           Rule r,
                           Transition t1,
                           Transition t2) {
		if (d.isZero())
			return false;

		boolean updated = false;
		if (sat.fa.add(d, t)) {
			workset.add(t);
            if (getGenerateTraceBuilder()) {
                traceBuilder.addAnnotation(t, r, d, t1, t2);
            }
			updated = true;
		}

		return updated;
	}

	/**
	 * A convenient method for
	 * {@@link #update(Semiring, Transition)}.
	 *
	 * @param d the sermiring value.
	 * @param p the transition's from-state
	 * @param a the transition's letter.
	 * @param q the transition's to-state
     * @param r the rule justifying the transition,
     * @param t1 the first transition justifying the new one (or null)
     * @param t2 the second transition justifying the new one (or null)
	 * @return <code>true</code> iff the transition is new or the semiring
	 *			value is not already included.
	 */
	private boolean update(BDDSemiring d,
                           String p,
                           String a,
                           String q,
                           Rule r,
                           Transition t1,
                           Transition t2) {
		return update(d, new Transition(p, a, q), r, t1, t2);
	}

	/**
	 * Repeatedly removes an element from the workset and saturates.
	 */
	private void depleteWorkset() {

		while (!workset.isEmpty()) {

			if (monitor != null && monitor.isCanceled()) return;

			Transition t = (Transition) workset.remove();
			Semiring d;

			// For all rules beginning with <p,a>
			Set<Rule> rules = getPdsRules(t.p, t.a);
			if (rules == null)
				continue;

			// Gets the diff weight
			BDDSemiring diff = (BDDSemiring)sat.fa.getDiff(t);
			if (diff == null)
				continue;

            Set<Rule> rulesToAdd = new HashSet<Rule>();

			// Iterates for each matching rule
			for (Rule rule : rules) {
				String p = rule.left.p;
				String a = rule.left.w[0];
                String w[] = rule.right.w;

				// Pop rules are ignored
                // Rew rules
				// Normal rule
				if (w.length == 1) {
				    d = diff.prepend(rule.d, monitor);

                    NewRuleJustification j = getNewRuleJustification(rule);
                    if (j == null) {
					    if (update((BDDSemiring)d, p, a, t.q, rule, t, null))
						    updateListener(a);
                    } else {
                        if (update((BDDSemiring)d, p, a, t.q, j.r, j.t1, t))
                            updateListener(a);
                    }
				} else if (w.length == 2) {
                    d = diff.prepend(rule.d, monitor);
                    if (!d.isZero()) {
                        PreCallSemiring call
                            = new PreCallSemiring((BDDSemiring)d);
                        Rule r = new Rule(call, p, a, t.q, w[1]);
                        rulesToAdd.add(r);
                        addNewRuleJustification(r, rule, t);
                        Set<Transition> ts = sat.fa.getTransitions(t.q, w[1]);
                        if (ts != null) {
                            for (Transition oldt : ts) {
                                BDDSemiring ring
                                    = (BDDSemiring)sat.fa.getWeight(oldt);
                                Semiring newd = ring.prepend(call, monitor);
                                if(update((BDDSemiring)newd, p, a, oldt.q, rule, t, oldt))
                                    updateListener(a);
                            }
                        }
                    }
                }
			}
            diff.free();

            for (Rule r : rulesToAdd)
                addPdsRule(r);
		}

	}

	/**
	 * Computes pre* of the given fa.
	 *
	 * @@param fa the initial automaton.
	 * @@param monitor the monitor.
	 * @@return a result structure containing the saturated automaton and a
     * trace builder if requested
	 */
	public PreSatResult prestar(Fa fa, CancelMonitor monitor) {
		this.monitor = monitor;

        getPdsRules();

		sat = new PreSatResult(new Fa(), traceBuilder);

		for (Map.Entry<Transition, Semiring> e : fa.trans.entrySet()) {
			update((BDDSemiring)e.getValue().id(), e.getKey(), null, null, null);
		}

        // Process the pop transitions
        for (Rule r : getPopRules()) {
            BDDSemiring d = BDDSemiring.createPrePop(r.getWeight(),
                                                     manager,
                                                     monitor);
            update(d, r.left.p, r.left.w[0], r.right.p, r, null, null);
        }

		// Depletes the workset
		depleteWorkset();

		return sat;
	}



    private void updateListener(String l) {
        // TODO: don't know what this should do, but i'm not using it so meh...
    }


    /**
     * Save the justification for a new rule
     *
     * @param newR the new rule
     * @param oldR the justifying rule
     * @param t1 the justifying transition
     */
    private void addNewRuleJustification(Rule newR, Rule oldR, Transition t1) {
        newRuleJustifications.put(newR, new NewRuleJustification(oldR, t1));
    }


    /**
     * @param r the rule
     * @return the justification for the rule if it was added during saturation,
     * else null
     */
    private NewRuleJustification getNewRuleJustification(Rule r) {
        return newRuleJustifications.get(r);
    }
}

