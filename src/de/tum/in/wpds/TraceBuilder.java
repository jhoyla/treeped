/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * A class for counter example generation produced by PreSat
 */


package de.tum.in.wpds;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.javabdd.BDD;

import org.apache.log4j.Logger;

import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.VarManager;

public class TraceBuilder {

    static Logger logger = Logger.getLogger(TraceBuilder.class);

    private class AnnotationDelta {
        public Rule r;
        public BDDSemiring bdd;
        // justifying transitions
        public Transition t1;
        public Transition t2;

        /**
         * @param r the pushdown rule justifying the transition
         * @param bdd the newly satisfying bdd assignments
         * @param t1 the first justifying transition or null if pop rule
         * @param t2 the second justifying transition or null if not push rule
         */
        public AnnotationDelta(Rule r,
                               BDDSemiring bdd,
                               Transition t1,
                               Transition t2) {
            this.r = r;
            this.bdd= bdd.id();
            this.t1 = t1;
            this.t2 = t2;
        }

        /**
         * Call on all annotations when done
         */
        public void free() {
            bdd.free();
        }
    }


    /**
     * An annotation is a list of which bdd assignments were justified by which
     * rule, with the first element in the list being the earliest justification
     * and the last the most recent (i.e. relies on lots of other deltas)
     */
    private class Annotation {
        private LinkedList<AnnotationDelta> deltas
            = new LinkedList<AnnotationDelta>();

        public Annotation() { }

        /**
         * @param r the pushdown rule justifying the transition
         * @param bdd the newly satisfying bdd assignments
         * @param t1 the first justifying transition or null if pop rule
         * @param t2 the second justifying transition or null if not push rule
         */
        public void addDelta(Rule r,
                             BDDSemiring bdd,
                             Transition t1,
                             Transition t2) {
            deltas.addLast(new AnnotationDelta(r, bdd, t1, t2));
        }

        /**
         * Call on all annotations when done
         */
        public void free() {
            for (AnnotationDelta d : deltas)
                d.free();
        }

        public List<AnnotationDelta> getDeltas() {
            return deltas;
        }
    }

    /**
     * Build traces by keeping a stack of info mimicking the stack of the pds
     */
    private class TraceStackElement {
        public BDDSemiring bdd;
        public Transition t;

        /**
         * @param bdd the valuation of the variables bdd(G0, L0, G1)
         * @param t the transition accepting this stack element
         */
        public TraceStackElement(BDDSemiring bdd,
                                 Transition t) {
            this.bdd = bdd;
            this.t = t;
        }
    }

    private VarManager manager;
    private Map<Transition, Annotation> annotations
        = new HashMap<Transition, Annotation>();

    /**
     * Construct a trace builder -- when transitions are added call
     * addAnnotation() to add annotations.
     *
     * @param manager the bdd variable manager to use
     */
    public TraceBuilder(VarManager manager) {
        this.manager = manager;
    }

    /**
     * Add an annotation to a transition.  These should be added in the order
     * that they're generated.
     *
     * @param t the transition to annotate
     * @param r the pushdown rule justifying the transition
     * @param bdd the newly satisfying bdd assignments
     * @param t1 the first justifying transition or null if pop rule
     * @param t2 the second justifying transition or null if not push rule
     */
    public void addAnnotation(Transition t,
                              Rule r,
                              BDDSemiring bdd,
                              Transition t1,
                              Transition t2) {
        Annotation a = annotations.get(t);
        if (a == null) {
            a = new Annotation();
            annotations.put(t, a);
        }
        a.addDelta(r, bdd, t1, t2);
    }


    /**
     * Call this function when done with the trace builder
     */
    public void free() {
        for (Annotation a : annotations.values())
            a.free();
    }



    /**
     * Note the bdd "init" is consumed by the algorithm
     *
     * @param init the bdd valuing the initial boolean variables
     * note, the algorithm relies on init having a single value for the local
     * and global variables (i.e. no l1 = 0 or l1 = 2...)
     * @param t the transition accepting the (one-character) stack (control
     * state and stack char implied by this)
     * @return a list of pushdown rules to apply to reach the target from the
     * given configuration (p, a) with init valuing the boolean vars
     */
    public List<Rule> getTrace(BDDSemiring init,
                               Transition t) {
        List<Rule> witness = new LinkedList<Rule>();
        Deque<TraceStackElement> stack = new ArrayDeque<TraceStackElement>();
        stack.push(new TraceStackElement(init, t));
        getTrace(stack, witness);
        return witness;
    }


    /**
     * @param stack the stack with trace information
     * @param witness the witness being built, new rules added to the end
     */
    private void getTrace(Deque<TraceStackElement> stack,
                          List<Rule> witness) {
        // keep looping until we exit (in the middle of the loop!) after finding
        // the final rule (a null one)
        while (true) {
            if (stack.isEmpty()) {
                logger.error("TraceBuilder ran out of stack in getTrace!");
                System.exit(-1);
            }

            TraceStackElement e = stack.pop();
            Annotation annot = getAnnotation(e.t);

            AnnotationDelta delta = getFirstDelta(annot, e);

            if (delta == null) {
                logger.error("TraceBuilder couldn't find a matching delta!");
                System.exit(-1);
            }

            // null rule means original transition and we're done
            if (delta.r == null)
                return;

            witness.add(delta.r);

            // note: we free e.bdd here (consumed by andWith)
            BDDSemiring vals = (BDDSemiring)delta.bdd.id().andWith(e.bdd);

            switch (delta.r.right.w.length) {
            case 0:
                // restrict the current top of stack to match current globals
                TraceStackElement newTop = stack.peek();
                newTop.bdd.andWith(vals.applyPop(delta.r.d, null));
                break;

            case 1:
                e.bdd = vals.extend(delta.r.d, null);
                e.t = delta.t1;
                stack.push(e);
                break;

            case 2:
                // pushRing: G2L2 is top of stack and control
                //           L0 is second to top of stack
                //           G1, L1 is choices global underneath top two chars
                //           consistent with transitions
                BDDSemiring pushRing = vals.extend(delta.r.d, null);

                // G2L2 are the new values -> rename to G0L0
                // G1 is not restricted
                // L0 is the value of the locals underneath -> rename to L1
                BDD topStack = manager.abstractG1L1(pushRing.bdd);
                topStack.replaceWith(manager.getG2pairG0());
                topStack.replaceWith(manager.getL0pairL1());
                topStack.replaceWith(manager.getL2pairL0());

                // G0 is not restricted
                // L0 is still L0 from pushRing
                // G1L1 are the same from pushRing too
                // G2L2 are not needed
                BDD nextStack = manager.abstractG2L2(pushRing.bdd);
                pushRing.free();

                stack.push(new TraceStackElement(new BDDSemiring(manager, nextStack),
                                                 delta.t2));
                stack.push(new TraceStackElement(new BDDSemiring(manager, topStack),
                                                 delta.t1));

                break;

            default:
                logger.error("TraceBuilder found a rule with too many RHS chars!");
                logger.error(delta.r);
                System.exit(-1);

            }

            vals.free();
        }
    }


    /**
     * @param t the transition
     * @return the annotation for the transition (or null)
     */
    private Annotation getAnnotation(Transition t) {
        return annotations.get(t);
    }


    private AnnotationDelta getFirstDelta(Annotation a, TraceStackElement e) {
        for (AnnotationDelta d : a.getDeltas()) {
            BDD match = d.bdd.bdd.id().andWith(e.bdd.bdd.id());
            if (!match.isZero()) {
                match.free();
                return d;
            }
            match.free();
        }
        return null;
    }
}
