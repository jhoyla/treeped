/* jQuery stub */



function $(arg, cont) {
    TAJS_addContextSensitivity("arg");
    TAJS_addContextSensitivity("cont");
    return new jQuery(arg, cont);
}

function jQuery(arg, cont) {
    TAJS_addContextSensitivity("arg");
    TAJS_addContextSensitivity("cont");

    if (arg == document) {
        this.arg = "$(document)";
    } else if (arg instanceof jQuery) {
        this.arg = arg.arg;
    } else if (arg != null) {
        if (cont != null && cont instanceof jQuery) {
            this.arg = cont.arg + ".find(\"" + arg + "\")";
        } else {
            this.arg = "$(\"" + arg + "\")";
        }
    }


    this.addClass = function(c) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("c");
        return this;
    };

    this.children = function(x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        if (x) {
            var n = new jQuery(this);
            n.arg +=".children(\"" + x + "\")";
            return n;
        } else {
            var n = new jQuery(this);
            n.arg +=".children()";
            return n;
        }
    };

    this.each = function (f) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("f");
        TAJS_addContextSensitivity(f, "this");
        f.apply(this, [0, this]);
        return this;
    };

    this.end = function () {
        TAJS_addContextSensitivity("this");
        var n = new jQuery(this);
        n.arg += ".end()";
        return n;
    };

    this.filter = function (x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        var n = new jQuery(this);
        n.arg +=".filter(\"" + x + "\")";
        return n;
    };

    this.find = function (x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        var n = new jQuery(this);
        n.arg +=".find(\"" + x + "\")";
        return n;
    };
    
    this.fadeIn = function (speed) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("speed");
        // do nothing
        return this;
    };

    this.has = function(x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        var n = new jQuery(this);
        n.arg +=".has(\"" + x + "\")";
        return n;
    };

    this.hover = function (fin, fout) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("fin");
        TAJS_addContextSensitivity("fout");
        fin.apply(this);
        fout.apply(this);
        return this;
    };

    this.next = function () {
        TAJS_addContextSensitivity("this");
        var n = new jQuery(this);
        n.arg +=".next()";
        return n;
    };

    this.parent = function(x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        var n = new jQuery(this);
        n.arg +=".parent()";
        return n;
    };

    this.parents = function(x) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("x");
        var n = new jQuery(this);
        n.arg +=".parents(\"" + x + "\")";
        return n;
    };

     // from zepto
    this.ready = function (callback) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("callback");
        document.addEventListener('DOMContentLoaded', callback, false)
        return this
    };

    this.removeClass = function (c) {
        TAJS_addContextSensitivity("this");
        TAJS_addContextSensitivity("c");
        alert(this.arg + ".removeClass(\"" + c + "\")");
        return this;
    };

    return this;
};

$.fn = jQuery.prototype = {
}; 

