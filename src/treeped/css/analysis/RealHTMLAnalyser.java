/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.analysis;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.w3c.dom.css.CSSRule;

import treeped.main.CmdOptions;
import treeped.util.MultiMap;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.RealToAbsHTMLTranslator;

import org.apache.log4j.Logger;

import com.steadystate.css.dom.AbstractCSSRuleImpl;

public class RealHTMLAnalyser {

    static Logger logger = Logger.getLogger(RealHTMLAnalyser.class);

    private static class ResAndTranlator {
        public HTMLAnalyser.AnalysisResult res;
        public RealToAbsHTMLTranslator translator;

        public ResAndTranlator(HTMLAnalyser.AnalysisResult res,
                               RealToAbsHTMLTranslator translator) {
            this.res = res;
            this.translator = translator;
        }

    }


    public static void analyseRealHTML(String fileName) {
        try {
            ResAndTranlator tr = doAnalyseHTML(fileName);

            // some redundancies might be css rules, so explain
            if (CmdOptions.getCheckCSSRules()) {
                MultiMap<CSSRule, String> redundantRules
                    = new MultiMap<CSSRule, String>();
                loadRedundantRules(tr, redundantRules);

                int totalRedundancies = 0;

                for (Map.Entry<CSSRule, Collection<String>> e
                        : redundantRules.entrySet()) {
                    printRedundantRule(e.getKey(), e.getValue());
                    totalRedundancies += e.getValue().size();
                }

                printTotalRedundancies(totalRedundancies);
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
    }


    /**
     * Analyses a collection of html files, and only reports css rules that are
     * redundant across all files
     *
     * @param htmlFiles
     */
    public static void analyseRealHTML(Collection<String> htmlFiles) {
        try {
            MultiMap<CSSRule, String> redundantRules =
                new MultiMap<CSSRule, String>();
            MultiMap<CSSRule, String> usedRules =
                new MultiMap<CSSRule, String>();

            for (String fileName : htmlFiles) {
                ResAndTranlator rt = doAnalyseHTML(fileName);

                if (CmdOptions.getCheckCSSRules()) {
                    loadRedundantRules(rt, redundantRules);
                    loadUsedRules(rt, usedRules);
                }
            }

            if (CmdOptions.getCheckCSSRules()) {
                int totalRedundancies = 0;
                for (Map.Entry<CSSRule, Collection<String>> e
                        : redundantRules.entrySet()) {
                    CSSRule r = e.getKey();

                    Collection<String> witnesses
                        = new HashSet<String>(e.getValue());

                    Collection<String> usedWitnesses = usedRules.get(r);
                    if (usedWitnesses != null)
                        witnesses.removeAll(usedWitnesses);

                    if (!witnesses.isEmpty()) {
                        printRedundantRule(r, witnesses);
                        totalRedundancies += witnesses.size();
                    }
                }

                printTotalRedundancies(totalRedundancies);
            }
        } catch(Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
    }

    private static ResAndTranlator doAnalyseHTML(String htmlFile)
            throws Exception {

        System.out.println("Analysing " + htmlFile);

        try {
            File f = new File(htmlFile);
            Document html = Jsoup.parse(f,
                                        "UTF-8",
                                        f.getParentFile().toURL().toString());

            RealToAbsHTMLTranslator t
                = new RealToAbsHTMLTranslator(html);

            if (CmdOptions.getOutputStats()) {
                System.out.println("Number of CSS selectors: " +
                                   t.getNumberOfCSSRulesAndSelectors());
            }

            CSSClass witnessClass = null;
            String witnessClassName = CmdOptions.getWitnessClass();
            if (witnessClassName != null)
                witnessClass = new CSSClass(witnessClassName);

            if (CmdOptions.getOutputAHtml()) {
                System.out.println("Abstract HTML:");
                System.out.println(t.getAbstractHTML());
            }

            HTMLAnalyser a = new HTMLAnalyser();
            HTMLAnalyser.AnalysisResult res
                = a.analyseHTML(t.getAbstractHTML(),
                                CmdOptions.getFullTree(),
                                witnessClass);

            if (CmdOptions.getOutputAHtmlResult()) {
                System.out.println("Result:");
                System.out.println(res);
            }

            if (CmdOptions.getOutputSimpleAHtml()) {
                System.out.println("Simple AHTML:");
                System.out.println(res.getSimpleAHtml());
            }

            if (CmdOptions.getOutputRemopla()) {
                System.out.println("Remopla:");
                System.out.println(res.getRemopla());
            }

            if (CmdOptions.getOutputPDS()) {
                System.out.println("Pds:");
                System.out.println(res.getPds());
            }

            if (witnessClass != null) {
                final Map<Rule<RuleGuard>,
                          Collection<RealToAbsHTMLTranslator.FileInfo>> fileInfo
                    = t.getFileInfo();
                final Map<Rule<SimpleRuleGuard>,
                          Rule<RuleGuard>> originalRuleMap = res.getOriginalRuleMap();

                System.out.println("Witness:");
                System.out.println(res.getWitness().toString(
                    new Witness.WitnessAnnotator() {
                        public String getRuleAnnotation(Rule<SimpleRuleGuard> r) {
                            if (originalRuleMap != null) {
                                Rule<RuleGuard> orig = originalRuleMap.get(r);
                                if (orig != null)
                                    return getFileInfo(orig);
                                return getFileInfo(orig);
                            } else {
                                return getFileInfo(r);
                            }
                        }

                        private String getFileInfo(Rule<? extends RuleGuard> r) {
                            Collection<RealToAbsHTMLTranslator.FileInfo> infos
                                = fileInfo.get(r);
                            if (infos != null) {
                                String annot = "";
                                for (RealToAbsHTMLTranslator.FileInfo i : infos) {
                                    annot += i.toString();
                                    annot += " ";
                                }
                                return annot;
                            }
                            return null;
                        }
                    }
                ));
            }

            return new ResAndTranlator(res, t);

        } catch (Exception e) {
            System.err.println("Error analysing " + htmlFile);
            System.err.println(e);
            e.printStackTrace();
            throw new Exception("Error analysing " + htmlFile);
        }
    }



    /**
     * Loads from a given analysis result the redundancies into the second arg.
     *
     * @param rt the resandtranslator from htmlanalyser
     * @param redundantRules a map from rules that are redundant to unused
     * classes that witness the redundancy
     */
    private static void loadRedundantRules(ResAndTranlator rt,
                                           MultiMap<CSSRule,
                                                    String> redundantRules) {
        Collection<CSSClass> redundancies = rt.res.getRedundancies();
        loadRuleClassesToMap(redundancies, rt.translator, redundantRules);
    }


    /**
     * Loads from analysis result the set of used CSS rules
     *
     * @param rt the result from htmlanalyser
     * @param usedRules a map from used rules to classes witnessing use
     */
    private static void loadUsedRules(ResAndTranlator rt,
                                      MultiMap<CSSRule, String> usedRules) {

        Collection<CSSClass> used = rt.res.getUsedClasses();
        loadRuleClassesToMap(used, rt.translator, usedRules);
    }

    /**
     * Helper function for loadUsedRules and loadRedundantRules
     *
     * @param classes a set of classes that are used or redundant
     * @param tr the realtoabshtmltranslator used
     * @param rules a map from rules to selectors (represented as strings)
     */
    private static void loadRuleClassesToMap(Collection<CSSClass> classes,
                                             RealToAbsHTMLTranslator tr,
                                             MultiMap<CSSRule, String> rules) {

        if (classes == null)
            return;

        for (CSSClass c : classes) {
            Collection<CSSRule> css = tr.getCSSRulesForClass(c);
            if (css != null) {
                for (CSSRule r : css) {
                    Collection<String> selectors
                        = tr.getSelectorsForClassRule(c, r);
                    if (selectors != null) {
                        rules.putAll(r, selectors);
                    }
                }
            }
        }

    }



    /**
     * @param r a css rule that is not used
     * @param witnesses the redundant classes that prove redundancy
     */
    private static void printRedundantRule(CSSRule r,
                                           Collection<String> witnesses) {
        System.out.println("Redundant selectors in rule:");
        System.out.println("  " + r);
        System.out.println("Unmatched selectors:");
        System.out.println("  " + witnesses);
    }

    /**
     * Print message to user saying total redundancies found in css.
     *
     * @param n the number of redundant css selectors
     */
    private static void printTotalRedundancies(int n) {
        if (n == 0)
            System.out.println("No redundant selectors found in CSS.");
        else
            System.out.println(n + " redundant selectors found in CSS.");
    }
}
