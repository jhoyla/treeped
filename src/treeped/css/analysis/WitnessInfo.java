/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Objects in which to store all the info needed for generating a counter
 * example.
 */

package treeped.css.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import treeped.css.representation.AHTMLTree;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleAddChild;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleVisitor;
import treeped.css.representation.SimpleRuleGuard;

public class WitnessInfo {

    static Logger logger = Logger.getLogger(WitnessInfo.class);

    public static class CSSClassJustif {
        // either a single rule or a pushdown justification
        private RuleApplication appliedRule = null;
        private PushdownJustification pushdownJustification = null;
        // the number of iterations of the fixed point algorithm required to add
        // this class
        private int iterationNumber;

        // construct an "initial" justification
        public CSSClassJustif() {
            this.iterationNumber = 0;
        }

        // construct a simple rule application justification
        public CSSClassJustif(RuleApplication appliedRule,
                              int iterationNumber) {
            this.appliedRule = appliedRule;
            this.iterationNumber = iterationNumber;
        }

        // construct a pushdown justification
        public CSSClassJustif(PushdownJustification pj,
                              int iterationNumber) {
            this.pushdownJustification = pj;
            this.iterationNumber = iterationNumber;
        }

        public boolean isInitialJustification() {
            return (appliedRule == null &&
                    pushdownJustification == null);
        }

        public boolean isSimpleJustification() {
            return (appliedRule != null);
        }

        public boolean isPushdownJustification() {
            return (pushdownJustification != null);
        }

        public RuleApplication getRuleApplication() {
            return appliedRule;
        }

        public PushdownJustification getPushdownJustification() {
            return pushdownJustification;
        }

        public int getIterationNumber() {
            return iterationNumber;
        }
    }

    private static class NodeClass {
        AHTMLTree node;
        CSSClass cssClass;

        public NodeClass(AHTMLTree node, CSSClass cssClass) {
            this.node = node;
            this.cssClass = cssClass;
        }

        public boolean equals(Object o) {
            if (o instanceof NodeClass) {
                NodeClass nc = (NodeClass)o;
                return (this.node.equals(nc.node) &&
                        this.cssClass.equals(nc.cssClass));
            }
            return false;
        }

        public int hashCode() {
            return node.hashCode() + cssClass.hashCode();
        }
    }

    // just to avoid calling new too many times
    private static NodeClass tmpNodeClass = new NodeClass(null, null);

    private static class WorklistItem implements Comparable<WorklistItem> {
        public AHTMLTree node;
        public CSSClass cssClass;
        public int iterationNumber;

        public WorklistItem(AHTMLTree node,
                            CSSClass cssClass,
                            int iterationNumber) {
            this.node = node;
            this.cssClass = cssClass;
            this.iterationNumber = iterationNumber;
        }

        public int compareTo(WorklistItem item) {
            return Integer.compare(iterationNumber, item.iterationNumber);
        }

        public boolean equals(Object o) {
            if (o instanceof WorklistItem) {
                WorklistItem i = (WorklistItem)o;
                return node.equals(i.node) && cssClass.equals(i.cssClass);
            }
            return false;
        }

        public int hashCode() {
            return node.hashCode() + cssClass.hashCode();
        }
    }

    private class Worklist {
        SortedSet<WorklistItem> list = new TreeSet<WorklistItem>();
        Set<NodeClass> done = new HashSet<NodeClass>();

        /**
         * Create an empty worklist
         */
        public Worklist() { }

        /**
         * Create a new worklist with an initial element
         */
        public Worklist(AHTMLTree node,
                        CSSClass cssClass) {
            int i = getIterationNumber(node, cssClass);
            list.add(new WorklistItem(node, cssClass, i));
        }

        public boolean isEmpty() {
            return list.isEmpty();
        }

        public WorklistItem next() {
            WorklistItem i = list.last();
            list.remove(i);
            done.add(new NodeClass(i.node, i.cssClass));
            return i;
        }

        public void addItem(AHTMLTree node, CSSClass cssClass) {
            tmpNodeClass.node = node;
            tmpNodeClass.cssClass = cssClass;
            if (!done.contains(tmpNodeClass)) {
                int i = getIterationNumber(node, cssClass);
                list.add(new WorklistItem(node, cssClass, i));
            }
        }
    }


    /**
     * Keep a map justifying classes at given nodes, but also one just
     * justifying a class being added anywhere in the tree
     */
    private Map<NodeClass, CSSClassJustif> justifications
        = new HashMap<NodeClass, CSSClassJustif>();
    private Map<CSSClass, CSSClassJustif> classJustifications
        = new HashMap<CSSClass, CSSClassJustif>();

    /**
     * Because a witness may create children we can't just use literal nodes of
     * the tree, so we use node ids, assigning each node of the original tree an
     * id, and giving any new nodes a new id.  These maps map between original
     * nodes and their ids
     */
    private Map<Integer, AHTMLTree> nodeIdToNodeMap
        = new HashMap<Integer, AHTMLTree>();
    private Map<AHTMLTree, Integer> nodeToNodeIdMap
        = new HashMap<AHTMLTree, Integer>();

    /**
     * The initial tree
     */
    private AHTMLTree initTree;

    private int nextNodeId = 0;

    /**
     * @param tree the initial tree witness generation starts from
     */
    public WitnessInfo(AHTMLTree tree) {
        initTree = tree;
        for (AHTMLTree node : tree.depthFirstIterator()) {
            allocateNodeId(node);
            for (CSSClass c : node.getCSSClasses()) {
                addInitialJustification(node, c);
            }
        }
    }


    /**
     * Mark a node as having a class from the start (don't need to use this if
     * passed initial tree to constructor)
     *
     * @param node the node
     * @param cssClass the class the node has
     */
    public void addInitialJustification(AHTMLTree node,
                                        CSSClass cssClass) {
        justifications.put(new NodeClass(node, cssClass),
                           new CSSClassJustif());
    }


    /**
     * For a rule applied directly to the tree without any pushdown analysis,
     * add a justification for the rule.
     *
     * Note: will not overwrite an existing justification
     *
     * @param node the node of the tree
     * @param child the child if the rule needs to match against a child (or
     * null if not)
     * @param cssClass the class added to the node
     * @param appliedRule the rule applied to add the class
     * @param iterationNumber the number of iterations of the algorithm applied
     * before adding this class to the node
     */
    public void addSimpleJustification(AHTMLTree node,
                                       AHTMLTree child,
                                       CSSClass cssClass,
                                       Rule<SimpleRuleGuard> appliedRule,
                                       int iterationNumber) {
        if (getJustification(node, cssClass) == null) {
            int upId = -1;
            int flatId = -1;
            int downId = -1;

            if (!node.isRoot() &&
                !appliedRule.getGuard().getUpClasses().isEmpty())
                upId = getNodeId(node.getParent());

            flatId = getNodeId(node);

            if (child != null &&
                !appliedRule.getGuard().getDownClasses().isEmpty())
                downId = getNodeId(child);

            RuleApplication app = new RuleApplication(appliedRule,
                                                      upId,
                                                      flatId,
                                                      downId,
                                                      -1);

            CSSClassJustif j = new CSSClassJustif(app, iterationNumber);

            justifications.put(new NodeClass(node, cssClass), j);

            if (!classJustifications.containsKey(cssClass)) {
                classJustifications.put(cssClass, j);
            }
        }
    }


    /**
     * Use this to add a justification that comes from a run of a pushdown
     * system.
     *
     * Note: will not clobber an existing justification
     *
     * @param node the node the class is being added to
     * @param cssClass the class added
     * @param pushdownJustification the pushdown justification from the class
     * adder
     * @param iterationNumber the number of iterations of the fixed point
     * algorithm that led to the introduction
     */
    public
    void addPushdownJustification(AHTMLTree node,
                                  CSSClass cssClass,
                                  PushdownJustification pushdownJustification,
                                  int iterationNumber) {
        if (getJustification(node, cssClass) == null) {
            CSSClassJustif j = new CSSClassJustif(pushdownJustification,
                                                  iterationNumber);

            justifications.put(new NodeClass(node, cssClass), j);

            if (!classJustifications.containsKey(cssClass)) {
                classJustifications.put(cssClass, j);
            }
        }
    }


    /**
     * Use this to add a justification that comes from a run of a pushdown
     * system and adds a class to some node that is not part of the original
     * tree.
     *
     * Note: will not clobber an existing justification
     *
     * @param cssClass the class added
     * @param pj the pushdown justification
     * @param iterationNumber the number of iterations of the fixed point
     * algorithm that led to the introduction
     */
    public void addNonRootPushdownJustification(CSSClass cssClass,
                                                PushdownJustification pj,
                                                int iterationNumber) {
        if (!classJustifications.containsKey(cssClass)) {
            CSSClassJustif j = new CSSClassJustif(pj, iterationNumber);
            classJustifications.put(cssClass, j);
        }
    }


    /**
     * @param node the node of the tree the class is added to
     * @param cssClass the class added
     * @return the full a list of rules, in order applied, to result in cssClass
     * being added to node, or null if no witness
     */
    public Witness getWitness(AHTMLTree node,
                              CSSClass cssClass) {

        CSSClassJustif j = getJustification(node, cssClass);
        return (j == null) ? null : getWitness(cssClass, j);
    }


    /**
     * @param cssClass the class you want at witness to being used
     * @return a list of rule applications resulting in the addition of cssClass
     * or null if class can't be added
     */
    public Witness getWitness(CSSClass cssClass) {
        CSSClassJustif j = classJustifications.get(cssClass);
        return (j == null) ? null : getWitness(cssClass, j);
    }

    /**
     * @param cssClass the class added
     * @param initJustification the initial justification from which to
     * construct the full witness
     * @return the rull list of rules, in order applied, to support the given
     * justification
     */
    private Witness getWitness(CSSClass cssClass,
                               CSSClassJustif initJustification) {
        List<RuleApplication> witness = new ArrayList<RuleApplication>();

        Worklist worklist = new Worklist();

        // j gets updated at the end of the loop
        CSSClassJustif j = initJustification;

        boolean emptyWorkList = false;
        // break near end
        while (true) {
            List<RuleApplication> appliedRules = getAppliedRules(j);

            // do nothing if there's nothing to be done
            if (appliedRules != null && !appliedRules.isEmpty()) {
                // is this really the best way?
                List<RuleApplication> newWitness
                    = new ArrayList<RuleApplication>(appliedRules);
                newWitness.addAll(witness);
                witness = newWitness;

                for (RuleApplication app : appliedRules) {
                    AHTMLTree appNode = getNode(app.flatNodeId);

                    // if it's not a node on the original tree, then the
                    // justification is taken care of as part of the pushdown run
                    if (appNode == null)
                        break;

                    // if has a parent it is also an original node
                    if (!appNode.isRoot()) {
                        AHTMLTree parent = appNode.getParent();
                        for (CSSClass c : app.rule.getGuard().getUpClasses()) {
                            worklist.addItem(parent, c);
                        }
                    }

                    for (CSSClass c : app.rule.getGuard().getFlatClasses()) {
                        worklist.addItem(appNode, c);
                    }

                    Collection<CSSClass> downClasses
                        = app.rule.getGuard().getDownClasses();
                    AHTMLTree child = getNode(app.downNodeId);

                    // if we have to justify child (i.e. not as part of a pushdown
                    // run)
                    if (!downClasses.isEmpty() && child != null) {
                        for (CSSClass c : downClasses) {
                            worklist.addItem(child, c);
                        }
                    }
                }
            }

            if (worklist.isEmpty())
                break;

            // set up for next loop
            WorklistItem i = worklist.next();
            j = getJustification(i.node, i.cssClass);
        };

        return new Witness(cssClass, witness, initTree, nodeToNodeIdMap);
    }


    private CSSClassJustif getJustification(AHTMLTree node, CSSClass cssClass) {
        tmpNodeClass.node = node;
        tmpNodeClass.cssClass = cssClass;
        return justifications.get(tmpNodeClass);
    }


    /**
     * @param node the node
     * @param cssClass the class
     * @return the iteration number where cssClass was added to node, or -1 if
     * never
     */
    private int getIterationNumber(AHTMLTree node,
                                   CSSClass cssClass) {
        CSSClassJustif j = getJustification(node, cssClass);
        return (j == null) ? -1 : j.iterationNumber;
    }


    /**
     * @return a fresh node id
     */
    private int getNextNodeId() {
        return nextNodeId++;
    }

    /**
     * @param node the node to allocate a new id to
     */
    private void allocateNodeId(AHTMLTree node) {
        int id = getNextNodeId();
        nodeIdToNodeMap.put(id, node);
        nodeToNodeIdMap.put(node, id);
    }

    /**
     * @param node the node
     * @return the id of a node or -1 if it doesn't have one (e.g. node wasn't
     * in the original tree)
     */
    private int getNodeId(AHTMLTree node) {
        Integer i = nodeToNodeIdMap.get(node);
        return (i == null) ? -1 : i;
    }

    /**
     * @param id the id
     * @return the node corresponding to a given id, or null if the id does not
     * match a node in the original tree
     */
    private AHTMLTree getNode(int id) {
        return nodeIdToNodeMap.get(id);
    }

    /**
     * Appends to the witness parameter a list of rule applications extracted
     * from the given list of pushdown rules
     *
     * @param parentId the id of the parent node
     * @param nodeId the id of the current node
     * @param childId the id of the designated child to whom all down rules will
     * be matched
     * @param appliedRules and iterator over list of pushdown rules that will be
     * moved along by the function, the iterator may not be done on return (i.e.
     * the pushdown system has popped more than it has pushed)
     * @param witness the list of rule applications to append the witness to
     */
    private
    void getPushdownWitness(final int parentId,
                            final int nodeId,
                            final int childId,
                            final Iterator<PushdownRule> appliedRules,
                            final List<RuleApplication> witness) {

        // after returning from an add child, all down rules will apply to the
        // new child rather than the original one
        // wrapped in an atomic integer for visitor pattern abuse
        final AtomicInteger lastChildId = new AtomicInteger(childId);

        while (appliedRules.hasNext()) {
            PushdownRule pr = appliedRules.next();

            if (pr.isPop())
                return;

            Rule<SimpleRuleGuard> r = pr.getRule();
            SimpleRuleGuard g = r.getGuard();

            int upId = -1;
            int downId = -1;

            if (!g.getUpClasses().isEmpty())
                upId = parentId;
            // note: we should never have lastChildId = -1 and a down class
            // being applied
            if (!g.getDownClasses().isEmpty())
                downId = lastChildId.get();

            // set up the rule to add (the visitor pattern below will fill in
            // the new child id (last arg) if needed
            final RuleApplication app = new RuleApplication(r,
                                                            upId,
                                                            nodeId,
                                                            downId,
                                                            -1);

            // process the next rule -- and add child requires a recursive call
            final WitnessInfo self = this;
            r.accept(new RuleVisitor<SimpleRuleGuard>() {
                public void visit(RuleAddClass<SimpleRuleGuard> r) {
                    witness.add(app);
                }

                public void visit(RuleAddChild<SimpleRuleGuard> r) {
                    lastChildId.set(getNextNodeId());
                    app.newChildId = lastChildId.get();
                    witness.add(app);
                    self.getPushdownWitness(nodeId,
                                            lastChildId.get(),
                                            -1,
                                            appliedRules,
                                            witness);
                }
            });
        }
    }


    /**
     * Gets the list of rule applications for a given justification at a given
     * node
     *
     * @param j the justification
     */
    private List<RuleApplication> getAppliedRules(CSSClassJustif j) {
        if (j.isInitialJustification()) {
            return null;
        } else if (j.isSimpleJustification()) {
            List<RuleApplication> apps = new ArrayList<RuleApplication>(1);
            apps.add(j.getRuleApplication());
            return apps;
        } else {
            PushdownJustification pj = j.getPushdownJustification();

            AHTMLTree node = pj.getNode();
            AHTMLTree child = pj.getChild();

            int parentId = -1;
            if (!node.isRoot())
                parentId = getNodeId(node.getParent());

            int nodeId = getNodeId(node);

            int childId = -1;
            if (child != null)
                childId = getNodeId(child);

            List<RuleApplication> apps = new LinkedList<RuleApplication>();

            getPushdownWitness(parentId,
                               nodeId,
                               childId,
                               pj.getWitness().iterator(),
                               apps);

            return apps;
        }
    }
}




