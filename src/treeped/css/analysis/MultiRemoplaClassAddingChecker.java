/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package treeped.css.analysis;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import net.sf.javabdd.BDD;

import de.tum.in.wpds.Fa;
import de.tum.in.wpds.Pds;
import de.tum.in.wpds.PreSat;
import de.tum.in.wpds.Semiring;
import de.tum.in.wpds.Transition;

import treeped.css.Namer;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.HTMLToMopedTranslator;
import treeped.css.translation.TranVars;
import treeped.exceptions.VariableNotFoundException;
import treeped.main.CmdOptions;
import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.RemoplaToPDS;
import treeped.remoplatopds.StmtOptimisations;
import treeped.remoplatopds.VarManager;
import treeped.representation.Remopla;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;
import treeped.translation.RemoplaAndOptimisations;


/**
 * Class adding checker that does a different check with a different version of
 * remopla for each different target CSS class.
 */
public class MultiRemoplaClassAddingChecker
extends ClassAddingChecker {

    /**
     * The remopla rule map, should probably be in the parent class, but
     * later...
     */
    private Map<String, Rule<SimpleRuleGuard>> ruleMap;

    /**
     * A class for storing the pds, remopla, &c. for checking a particular css
     * class
     */
    private class CSSClassState {
        public Pds pds;
        public Remopla remopla;
        public String nodeFun;
        public TranVars vars;
        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations;
        Map<String, Rule<SimpleRuleGuard>> ruleMap;


        public CSSClassState(MultiRemoplaClassAddingChecker classAdder,
                             Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
            this.pds = classAdder.pds;
            this.remopla = classAdder.remopla;
            this.nodeFun = classAdder.nodeFun;
            this.vars = classAdder.vars;
            this.stmtOptimisations = stmtOptimisations;
            this.ruleMap = classAdder.ruleMap;
        }

        public Map<RemoplaStmt, StmtOptimisations>
        restore(MultiRemoplaClassAddingChecker classAdder) {
            classAdder.pds = this.pds;
            classAdder.remopla = this.remopla;
            classAdder.nodeFun = this.nodeFun;
            classAdder.vars = this.vars;
            classAdder.ruleMap = this.ruleMap;
            return stmtOptimisations;
        }
    }

    static Logger logger = Logger.getLogger(MultiRemoplaClassAddingChecker.class);

    public MultiRemoplaClassAddingChecker(AbstractHTML<SimpleRuleGuard> ahtml)
    throws Exception {
        this.ahtml = ahtml;
    }


    public List<PushdownRule> getWitness(PushdownJustification justification) {
        AHTMLTree node = justification.getNode();
        if (justification.isFullTreeCheck()) {
            return doFullTreeGetWitness(justification.getAddClass(),
                                        justification.getNodeClasses(),
                                        justification.getParentClasses(),
                                        justification.getChildClasses(),
                                        node.isRoot());
        } else {
            return doGetWitness(justification.getAddClass(),
                                justification.getNodeClasses(),
                                justification.getParentClasses(),
                                justification.getChildClasses(),
                                node.isRoot());
        }
    }



    protected CheckResult doCheck(CSSClass addClass,
                                  Collection<CSSClass> initClasses,
                                  Collection<CSSClass> parentClasses,
                                  Collection<CSSClass> childClasses,
                                  boolean isRoot)
    throws Exception {
        initialiseForClass(addClass);

        //  + Build an automaton that accepts single element stack
        //    with addClass set to true
        //  + Do the pre*
        //  + True iff config that corresponds to I_{initClasses,
        //    parentClasses, isRoot} is accepted by the pre*

        Semiring possInit = getPossibleInitSemiring(addClass);

        // create the semiring describing the initial state we're testing
        BDD initBDD = createInitBDD(isRoot,
                                    initClasses,
                                    parentClasses,
                                    childClasses);
        BDDSemiring initRing = new BDDSemiring(manager, initBDD);

        Semiring res = possInit.id().andWith(initRing);

        boolean result = !res.isZero();

        res.free();

        CheckResult cres;
        if (getJustification()) {
            cres = new CheckResult(result,
                                   new PushdownJustification(addClass,
                                                             false,
                                                             this));
        } else {
            cres = new CheckResult(result);
        }

        return cres;
    }


    protected CheckResult doCheckFullTree(CSSClass addClass,
                                          Collection<CSSClass> initClasses,
                                          Collection<CSSClass> parentClasses,
                                          Collection<CSSClass> childClasses,
                                          boolean isRoot)
    throws Exception {

        initialiseForClass(addClass);

        //  + Build an automaton that accepts all stacks with ya = true on top.
        //  (and at least two stack elements, but that's only for convenience)
        //  + Do the pre*
        //  + True iff config that corresponds to I_{initClasses,
        //    parentClasses, isRoot} is accepted by the pre*

        Semiring possInit = getFullTreePossibleInitSemiring(addClass);

        // create the semiring describing the initial state we're testing
        BDD initBDD = createInitBDD(isRoot,
                                    initClasses,
                                    parentClasses,
                                    childClasses);
        BDDSemiring initRing = new BDDSemiring(manager, initBDD);

        Semiring res = possInit.id().andWith(initRing);

        boolean result = !res.isZero();

        res.free();


        CheckResult cres;
        if (getJustification()) {
            cres = new CheckResult(result,
                                   new PushdownJustification(addClass,
                                                             true,
                                                             this));
        } else {
            cres = new CheckResult(result);
        }
        return cres;
    }




    /**
     * Memoization of createPossibleInitSemiring
     */
    private Map<CSSClass, Semiring> possInitMemo = new HashMap<CSSClass, Semiring>();
    private Semiring getPossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {
        Semiring res = possInitMemo.get(addClass);
        if (res == null) {
            res = createPossibleInitSemiring(addClass);
            possInitMemo.put(addClass, res);
        }
        return res;
    }

    /**
     * Uses moped to get a semiring giving all possible initial variable
     * assignments that can eventually lead to the addition of class addClass at
     * the root node.
     *
     * @param addClass find representation of all initial variable assignments
     * that can lead to addClass being added to the root
     * @return a semiring representation of the above
     */
    private Semiring createPossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {

        // Initializes target FA
        BDD targetBDD = createTargetBDD(addClass);

        // the target aut just needs one transition
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, targetBDD),
               Fa.q_i,
               nodeFun,
               Fa.q_f);

        // pre*
        PreSat sat = new PreSat(pds, manager);
        PreSat.PreSatResult preFa = sat.prestar(fa, null);

        // get copy of result semiring
        Semiring res = preFa.fa.getWeight(Fa.q_i, nodeFun, Fa.q_f);

        if (res != null)
            res = res.id();
        else
            res = new BDDSemiring(manager, manager.zero());

        // free all others
        preFa.free();

        return res;
    }


    /**
     * Memoization of createFullTreePossibleInitSemiring
     */
    private Map<CSSClass, Semiring> fullTreePossInitMemo
        = new HashMap<CSSClass, Semiring>();

    private Semiring getFullTreePossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {
        Semiring res = fullTreePossInitMemo.get(addClass);
        if (res == null) {
            res = createFullTreePossibleInitSemiring(addClass);
            fullTreePossInitMemo.put(addClass, res);
        }
        return res;
    }

    private void clearMemo() {
        for (Semiring s : possInitMemo.values())
            s.free();
        for (Semiring s : fullTreePossInitMemo.values())
            s.free();

        possInitMemo.clear();
        fullTreePossInitMemo.clear();
    }

    /**
     * Uses moped to get a semiring giving all possible initial variable
     * assignments that can eventually lead to the addition of class addClass at
     * some child node except the root node.
     *
     * @param addClass find representation of all initial variable assignments
     * that can lead to addClass being added to some node except root
     * @return a semiring representation of the above
     */
    private Semiring createFullTreePossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {

        // Initializes target FA
        BDD targetBDD = createTargetBDD(addClass);

        BDD one = manager.initVars();
        String q = Namer.getMidState();

        // the target aut just needs
        //
        //   q0 -- targetBDD --> q -- one --> qf
        //                     /   \
        //                     |   |
        //                     \one/
        //
        //
        // that is class add true on top, and anything under
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, targetBDD),
               Fa.q_i,
               nodeFun,
               q);
        for (String l : remopla.getLabels()) {
            fa.add(new BDDSemiring(manager, one.id()),
                   q,
                   l,
                   q);
            fa.add(new BDDSemiring(manager, one.id()),
                   q,
                   nodeFun,
                   Fa.q_f);
        }
        one.free();

        // pre*
        PreSat sat = new PreSat(pds, manager);
        PreSat.PreSatResult preFa = sat.prestar(fa, null);

        // get copy of result semiring
        Semiring res = preFa.fa.getWeight(Fa.q_i, nodeFun, Fa.q_f);

        if (res != null)
            res = res.id();
        else
            res = new BDDSemiring(manager, manager.zero());

        // free all others
        preFa.free();

        return res;
    }






    /**
     * Create bdd for initial config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where
     *
     *      pop = pop
     *      root = isRoot
     *      x1,...,xn = childClasses
     *      y1,...,yn = initClasses
     *      z1,...,zn = parentClasses
     *
     * @param isRoot whether the parent of the node being tested is root
     * @param initClasses the classes the initial node has
     * @param parentClasses the classes its parent has
     * @param childClasses the classes its child has
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createInitBDD(boolean isRoot,
                              Collection<CSSClass> initClasses,
                              Collection<CSSClass> parentClasses,
                              Collection<CSSClass> childClasses)
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        for (CSSClass c : vars.getXVarClasses()) {
            manager.initBoolVar(vars.getXVar(c),
                                childClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getYVarClasses()) {
            manager.initBoolVar(vars.getYVar(c),
                                initClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getZVarClasses()) {
            manager.initBoolVar(vars.getZVar(c),
                                parentClasses.contains(c),
                                bdd);
        }

        manager.initBoolVar(vars.getRootVar(), isRoot, bdd);
        manager.initBoolVar(vars.getPopVar(), true, bdd);

        return bdd;
    }


    /**
     * Create bdd for target config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where yi for class addClass is true (anything else can be anything)
     *
     * @param addClass the classes to add
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createTargetBDD(CSSClass addClass)
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        manager.initBoolVar(vars.getYVar(addClass),
                            true,
                            bdd);

        return bdd;
    }

    /**
     * Initialises the class adding checker for adding the given class, memoised
     * with initialiseMemo
     *
     * @param c a css class
     */
    private Map<CSSClass, CSSClassState> initialiseMemo
        = new HashMap<CSSClass, CSSClassState>();

    private void initialiseForClass(CSSClass c) throws Exception {
        HTMLToMopedTranslator.RemoplaAndVars rvs = null;

        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations;

        CSSClassState memo = initialiseMemo.get(c);
        if (memo == null) {
            Set<CSSClass> cs = new HashSet<CSSClass>();
            cs.add(c);

            HTMLToMopedTranslator tr = new HTMLToMopedTranslator();
            rvs = tr.translateSimpleHTML(ahtml, cs);

            this.remopla = rvs.remopt.remopla;
            stmtOptimisations = rvs.remopt.stmtOptimisations;
            ruleMap = rvs.ruleMap;

            this.vars = rvs.vars;
            this.pds = RemoplaToPDS.translate(this.remopla, stmtOptimisations);

            // initial fun label
            RemoplaMethod nodeFunMethod
                = this.remopla.getMethod(Namer.getNodeMethodName());
            this.nodeFun = nodeFunMethod.getStmts().get(0).getLabel();

            initialiseMemo.put(c, new CSSClassState(this, stmtOptimisations));
        } else {
            stmtOptimisations = memo.restore(this);
        }

        // Creates variable manager (no need to recreate since remopla always
        // has same number of vars)
        if (this.manager == null) {
            this.manager = new VarManager(CmdOptions.getBddPackage(),
                                          CmdOptions.getNodeNum(),
                                          CmdOptions.getCacheSize(),
                                          1, // no integers, just bools
                                          this.remopla,
                                          stmtOptimisations);
        } else {
            boolean refresh = this.manager.setRemopla(this.remopla,
                                                      stmtOptimisations);
            if (refresh) {
                clearMemo();
            }
        }
    }



    /**
     * Does the witness thing, but gets a witness as proof.  Same arguments as
     * doCheck.
     */
    protected List<PushdownRule> doGetWitness(CSSClass addClass,
                                              Collection<CSSClass> initClasses,
                                              Collection<CSSClass> parentClasses,
                                              Collection<CSSClass> childClasses,
                                              boolean isRoot) {
        try {
            // horrible copy paste of doCheck code...
            initialiseForClass(addClass);

            // Initializes target FA
            BDD targetBDD = createTargetBDD(addClass);
            // the target aut just needs one transition
            Fa fa = new Fa();
            fa.add(new BDDSemiring(manager, targetBDD),
                   Fa.q_i,
                   nodeFun,
                   Fa.q_f);

            // pre*
            PreSat sat = new PreSat(pds, manager);
            sat.setGenerateTraceBuilder(true);
            PreSat.PreSatResult preFa = sat.prestar(fa, null);

            BDD initBDD = createInitBDD(isRoot,
                                        initClasses,
                                        parentClasses,
                                        childClasses);
            BDDSemiring initRing = new BDDSemiring(manager, initBDD);

            Transition initT = new Transition(Fa.q_i, nodeFun, Fa.q_f);

            List<de.tum.in.wpds.Rule> trace
                = preFa.traceBuilder.getTrace(initRing, initT);

            List<PushdownRule> witness = getWitnessFromPushdownRules(trace);

            preFa.free();

            return witness;
        } catch (Exception e) {
            logger.error("Exception during witness generation, giving up.", e);
            System.exit(-1);
            return null;
        }
    }


    /**
     * Does the witness thing, but gets a witness as proof.  Same arguments as
     * doFullTreeCheck.
     */
    private
    List<PushdownRule> doFullTreeGetWitness(CSSClass addClass,
                                            Collection<CSSClass> initClasses,
                                            Collection<CSSClass> parentClasses,
                                            Collection<CSSClass> childClasses,
                                            boolean isRoot) {
        try {
            // copy-paste of full tree check code
            initialiseForClass(addClass);

            BDD targetBDD = createTargetBDD(addClass);

            BDD one = manager.initVars();
            String q = Namer.getMidState();

            Fa fa = new Fa();
            fa.add(new BDDSemiring(manager, targetBDD),
                   Fa.q_i,
                   nodeFun,
                   q);
            for (String l : remopla.getLabels()) {
                fa.add(new BDDSemiring(manager, one.id()),
                       q,
                       l,
                       q);
                fa.add(new BDDSemiring(manager, one.id()),
                       q,
                       nodeFun,
                       Fa.q_f);
            }
            one.free();

            // pre*
            PreSat sat = new PreSat(pds, manager);
            sat.setGenerateTraceBuilder(true);
            PreSat.PreSatResult preFa = sat.prestar(fa, null);

            BDD initBDD = createInitBDD(isRoot,
                                        initClasses,
                                        parentClasses,
                                        childClasses);
            BDDSemiring initRing = new BDDSemiring(manager, initBDD);

            Transition initT = new Transition(Fa.q_i, nodeFun, Fa.q_f);

            List<de.tum.in.wpds.Rule> trace
                = preFa.traceBuilder.getTrace(initRing, initT);

            List<PushdownRule> witness = getWitnessFromPushdownRules(trace);

            preFa.free();

            return witness;
        } catch (Exception e) {
            logger.error("Exception while creating full tree witness: ", e);
            System.exit(-1);
            return null;
        }
    }


    /**
     * @param trace a trace of rules of the pusdown system
     * @return a list of rules of the tree (or pop rules)
     */
    private List<PushdownRule> getWitnessFromPushdownRules(List<de.tum.in.wpds.Rule> trace) {
        List<PushdownRule> witness = new LinkedList<PushdownRule>();
        for (de.tum.in.wpds.Rule r : trace) {
            if (r.right.w.length == 0) {
                // pop rule
                witness.add(new PushdownRule());
            } else {
                String label = r.left.w[0];
                Rule<SimpleRuleGuard> tr = ruleMap.get(label);
                if (tr != null)
                    witness.add(new PushdownRule(tr));
            }
        }

        return witness;
    }
}
