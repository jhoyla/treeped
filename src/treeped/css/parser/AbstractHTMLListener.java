// Generated from src/treeped/css/parser/AbstractHTML.g4 by ANTLR 4.2.2
package treeped.css.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AbstractHTMLParser}.
 */
public interface AbstractHTMLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#UpPlusGuard}.
	 * @param ctx the parse tree
	 */
	void enterUpPlusGuard(@NotNull AbstractHTMLParser.UpPlusGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#UpPlusGuard}.
	 * @param ctx the parse tree
	 */
	void exitUpPlusGuard(@NotNull AbstractHTMLParser.UpPlusGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#abshtmltree}.
	 * @param ctx the parse tree
	 */
	void enterAbshtmltree(@NotNull AbstractHTMLParser.AbshtmltreeContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#abshtmltree}.
	 * @param ctx the parse tree
	 */
	void exitAbshtmltree(@NotNull AbstractHTMLParser.AbshtmltreeContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#OrGuard}.
	 * @param ctx the parse tree
	 */
	void enterOrGuard(@NotNull AbstractHTMLParser.OrGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#OrGuard}.
	 * @param ctx the parse tree
	 */
	void exitOrGuard(@NotNull AbstractHTMLParser.OrGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#UpGuard}.
	 * @param ctx the parse tree
	 */
	void enterUpGuard(@NotNull AbstractHTMLParser.UpGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#UpGuard}.
	 * @param ctx the parse tree
	 */
	void exitUpGuard(@NotNull AbstractHTMLParser.UpGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#UpStarGuard}.
	 * @param ctx the parse tree
	 */
	void enterUpStarGuard(@NotNull AbstractHTMLParser.UpStarGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#UpStarGuard}.
	 * @param ctx the parse tree
	 */
	void exitUpStarGuard(@NotNull AbstractHTMLParser.UpStarGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#AndGuard}.
	 * @param ctx the parse tree
	 */
	void enterAndGuard(@NotNull AbstractHTMLParser.AndGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#AndGuard}.
	 * @param ctx the parse tree
	 */
	void exitAndGuard(@NotNull AbstractHTMLParser.AndGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#DownPlusGuard}.
	 * @param ctx the parse tree
	 */
	void enterDownPlusGuard(@NotNull AbstractHTMLParser.DownPlusGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#DownPlusGuard}.
	 * @param ctx the parse tree
	 */
	void exitDownPlusGuard(@NotNull AbstractHTMLParser.DownPlusGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#downguardr}.
	 * @param ctx the parse tree
	 */
	void enterDownguardr(@NotNull AbstractHTMLParser.DownguardrContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#downguardr}.
	 * @param ctx the parse tree
	 */
	void exitDownguardr(@NotNull AbstractHTMLParser.DownguardrContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#DownGuard}.
	 * @param ctx the parse tree
	 */
	void enterDownGuard(@NotNull AbstractHTMLParser.DownGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#DownGuard}.
	 * @param ctx the parse tree
	 */
	void exitDownGuard(@NotNull AbstractHTMLParser.DownGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#flatguardr}.
	 * @param ctx the parse tree
	 */
	void enterFlatguardr(@NotNull AbstractHTMLParser.FlatguardrContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#flatguardr}.
	 * @param ctx the parse tree
	 */
	void exitFlatguardr(@NotNull AbstractHTMLParser.FlatguardrContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(@NotNull AbstractHTMLParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(@NotNull AbstractHTMLParser.ProgContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#AddChild}.
	 * @param ctx the parse tree
	 */
	void enterAddChild(@NotNull AbstractHTMLParser.AddChildContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#AddChild}.
	 * @param ctx the parse tree
	 */
	void exitAddChild(@NotNull AbstractHTMLParser.AddChildContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#classSet}.
	 * @param ctx the parse tree
	 */
	void enterClassSet(@NotNull AbstractHTMLParser.ClassSetContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#classSet}.
	 * @param ctx the parse tree
	 */
	void exitClassSet(@NotNull AbstractHTMLParser.ClassSetContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#SimpleGuard}.
	 * @param ctx the parse tree
	 */
	void enterSimpleGuard(@NotNull AbstractHTMLParser.SimpleGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#SimpleGuard}.
	 * @param ctx the parse tree
	 */
	void exitSimpleGuard(@NotNull AbstractHTMLParser.SimpleGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#upguardr}.
	 * @param ctx the parse tree
	 */
	void enterUpguardr(@NotNull AbstractHTMLParser.UpguardrContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#upguardr}.
	 * @param ctx the parse tree
	 */
	void exitUpguardr(@NotNull AbstractHTMLParser.UpguardrContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#TrueGuard}.
	 * @param ctx the parse tree
	 */
	void enterTrueGuard(@NotNull AbstractHTMLParser.TrueGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#TrueGuard}.
	 * @param ctx the parse tree
	 */
	void exitTrueGuard(@NotNull AbstractHTMLParser.TrueGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#DownStarGuard}.
	 * @param ctx the parse tree
	 */
	void enterDownStarGuard(@NotNull AbstractHTMLParser.DownStarGuardContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#DownStarGuard}.
	 * @param ctx the parse tree
	 */
	void exitDownStarGuard(@NotNull AbstractHTMLParser.DownStarGuardContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#nodeclassdecl}.
	 * @param ctx the parse tree
	 */
	void enterNodeclassdecl(@NotNull AbstractHTMLParser.NodeclassdeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#nodeclassdecl}.
	 * @param ctx the parse tree
	 */
	void exitNodeclassdecl(@NotNull AbstractHTMLParser.NodeclassdeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#AHTMLTree}.
	 * @param ctx the parse tree
	 */
	void enterAHTMLTree(@NotNull AbstractHTMLParser.AHTMLTreeContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#AHTMLTree}.
	 * @param ctx the parse tree
	 */
	void exitAHTMLTree(@NotNull AbstractHTMLParser.AHTMLTreeContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#CSSClasses}.
	 * @param ctx the parse tree
	 */
	void enterCSSClasses(@NotNull AbstractHTMLParser.CSSClassesContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#CSSClasses}.
	 * @param ctx the parse tree
	 */
	void exitCSSClasses(@NotNull AbstractHTMLParser.CSSClassesContext ctx);

	/**
	 * Enter a parse tree produced by {@link AbstractHTMLParser#AddClass}.
	 * @param ctx the parse tree
	 */
	void enterAddClass(@NotNull AbstractHTMLParser.AddClassContext ctx);
	/**
	 * Exit a parse tree produced by {@link AbstractHTMLParser#AddClass}.
	 * @param ctx the parse tree
	 */
	void exitAddClass(@NotNull AbstractHTMLParser.AddClassContext ctx);
}