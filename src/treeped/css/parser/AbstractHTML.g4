

grammar AbstractHTML;

prog: (abshtmlrule)* fullabshtmltree? cssclassesr?;

abshtmlrule:
      guard=abshtmlguard ',' 'AddClass(' changeClasses=classSet ')' # AddClass
    | guard=abshtmlguard ',' 'AddChild(' changeClasses=classSet ')' # AddChild
    ;

abshtmlguard: 
      up=upguardr flat=flatguardr? down=downguardr?  # SimpleGuard
    |             flat=flatguardr  down=downguardr?  # SimpleGuard
    |                              down=downguardr   # SimpleGuard
    | 'Up+' abshtmlguard                             # UpPlusGuard
    | 'Down+' abshtmlguard                           # DownPlusGuard
    | 'Up*' abshtmlguard                             # UpStarGuard
    | 'Down*' abshtmlguard                           # DownStarGuard
    | 'Up' abshtmlguard                              # UpGuard
    | 'Down' abshtmlguard                            # DownGuard
    | 'And' '{' abshtmlguard* '}'                    # AndGuard
    | 'Or' '{' abshtmlguard* '}'                     # OrGuard
    | 'True'                                         # TrueGuard
    ;

upguardr: 'Up' guardClasses=classSet;

flatguardr: guardClasses=classSet;

downguardr: 'Down' guardClasses=classSet;

classSet: '{' ID* '}' ;

fullabshtmltree: 
    rootNode=abshtmltree # AHTMLTree
    ;

abshtmltree:
      '<' 'node' nodeClasses=nodeclassdecl? '/>' 
    | '<' 'node' nodeClasses=nodeclassdecl? '>' nodeChildren=abshtmltree* '</' 'node' '>'
    ;

nodeclassdecl: 
    'class' '=' '"' ID* '"';

cssclassesr: 
    'CSS Classes:' classSet # CSSClasses
    ;

ID : [a-zA-Z][a-zA-Z0-9_-#]* ;

WS : ([ \t\r\n\f]+ | '/*' (~'*' | '*' ~'/')* '*/') -> channel(HIDDEN) ;
