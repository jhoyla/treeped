/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Namer.java
// Author: Matthew Hague
//
// The namer class provides the naming conventions used in the translations.

package treeped.css;

import treeped.css.representation.CSSClass;

public class Namer {

    private static final String HTML_EXT = ".ahtml";
    private static final String REAL_HTML_EXT = ".html";

    private static final String ROOT_VAR = "root";
    private static final String POP_VAR = "pop";
    private static final String X_VAR_PREFIX = "x";
    private static final String Y_VAR_PREFIX = "y";
    private static final String Z_VAR_PREFIX = "z";
    private static final String INTERESTING_CLASS_PREFIX = "_IC";

    private static final String RULEGUARD_CSSCLASS_PREFIX = "_RG";
    private static final String HIDDEN_CSSCLASS_PREFIX = "_";
    private static final String BUILD_TREE_PREFIX = "_tb_";
    private static final String CSS_RULE_CSSCLASS_PREFIX = "_cssrule";

    private static final String NODE_METHOD_NAME = "nodeFun";

    private static final String MID_STATE = "qmid";

    private static final String ANY_TREE_CLASS = "_any_";
    private static final String DOC_CLASS = "<html";

    private static final String ID_HASH = "#";
    private static final String TAG_PREFIX = "<";

    private static final String RULE_LABEL_PREFIX = "rule";

    public static boolean isHTMLFile(String fileName) {
        return fileName.endsWith(HTML_EXT);
    }

    public static boolean isRealHTMLFile(String fileName) {
        return fileName.endsWith(REAL_HTML_EXT);
    }

    public static String getAnyTreeClass() {
        return ANY_TREE_CLASS;
    }


    public static String getDocumentClass() {
        return DOC_CLASS;
    }

    /**
     * @param c a css class
     * @return the class name in the ahtml
     */
    public static String getClassClassName(String c) {
        return c;
    }

    public static String getIDClassName(String id) {
        return ID_HASH + id;
    }

    public static String getTagClassName(String tag) {
        return TAG_PREFIX + tag;
    }

    /**
     * A class name corresponding to the call of a jquery function such as remove
     */
    public static String getJQueryCallClass(String funName) {
        return "_" + funName + "_";
    }


    /**
     * @return the name of the root var
     */
    public static String getRootVar() {
        return ROOT_VAR;
    }

    /**
     * @return the name of the pop var
     */
    public static String getPopVar() {
        return POP_VAR;
    }

    /**
     * @param i index
     * @return the name of the ith x variable
     */
    public static String getXVar(int i) {
        return X_VAR_PREFIX + i;
    }

    /**
     * @param i the index
     * @return the name of the ith y variable
     */
    public static String getYVar(int i) {
        return Y_VAR_PREFIX + i;
    }

    /**
     * @param i index
     * @return the name of the ith z variable
     */
    public static String getZVar(int i) {
        return Z_VAR_PREFIX + i;
    }

    /**
     * @param i index
     * @return string for the ith interesting class name
     */
    public static String getInterestingClassName(int i) {
        return INTERESTING_CLASS_PREFIX + i;
    }


    /**
     * @return the name of the method that does the simulating
     */
    public static String getNodeMethodName() {
        return NODE_METHOD_NAME;
    }

    /**
     * @return a prefix for naming classes used for rule guards in
     * AHTMLToSimpleAHTML, guaranteed not to be used from the user input.
     */
    public static String getRuleGuardCSSClassPrefix() {
        return RULEGUARD_CSSCLASS_PREFIX;
    }


    /**
     * @param cssName the name of a css class
     * @return a modified version of the name that will not appear as part of
     * user input
     */
    public static String getHiddenName(String cssName) {
        return HIDDEN_CSSCLASS_PREFIX + cssName;
    }

    /**
     * @return a state name for the FA in class adding checkers to act as
     * intermediate state in full tree check
     */
    public static String getMidState() {
        return MID_STATE;
    }

    /**
     * @return a class name to use for building trees in translation of
     * append/html jquery calls -- unique names are needed in the guards of
     * rules that construct the tree
     */
    public static String getBuildTreeClassName(int id) {
        return BUILD_TREE_PREFIX + id;
    }

    public static String getRuleStatementLabel(int i) {
        return RULE_LABEL_PREFIX + i;
    }


    public static String getCSSRuleClassName(int i) {
        return CSS_RULE_CSSCLASS_PREFIX + i;
    }
}
