/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* ToMopedAnalysis class for CSS To Moped conversion
 * Written by: Matthew Hague
 */

package treeped.css.translation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import treeped.css.Namer;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleAddChild;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleVisitor;
import treeped.css.representation.SimpleRuleGuard;
import treeped.remoplatopds.StmtOptimisations;
import treeped.representation.Remopla;
import treeped.representation.RemoplaExpr;
import treeped.representation.RemoplaAssignStmt;
import treeped.representation.RemoplaDoStmt;
import treeped.representation.RemoplaBoolVar;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaVar;
import treeped.translation.RemoplaAndOptimisations;

import org.apache.log4j.Logger;

import static treeped.representation.RemoplaStaticFactory.*;

public class HTMLToMopedTranslator {

    static Logger logger = Logger.getLogger(HTMLToMopedTranslator.class);

    public class RemoplaAndVars {
        public RemoplaAndOptimisations remopt;
        public TranVars vars;
        /**
         * A partial map from labels to rules of the ahtml (unmapped
         * rules can be ignored)
         */
        public Map<String, Rule<SimpleRuleGuard>> ruleMap;

        public RemoplaAndVars(RemoplaAndOptimisations remopt,
                              TranVars vars,
                              Map<String, Rule<SimpleRuleGuard>> ruleMap) {
            this.remopt = remopt;
            this.vars = vars;
            this.ruleMap = ruleMap;
        }
    }


    /** Translates the ahtml with simple rule guards into remopla.
     *  @param ahtml AbstractHTML object representing ahtml to translate
     *  @param interestingClasses css classes that might not be pertinent to the
     *  execution, but we still want to know if they're added or not (all
     *  classes you're interested in should be in here)
     *  @return the Remopla object corresponding to the css -- the main
     *  function will be the entry point
     */
    public RemoplaAndVars
        translateSimpleHTML(AbstractHTML<SimpleRuleGuard> ahtml,
                            Collection<CSSClass> interestingClasses)
    throws Exception {
        return translateSimpleHTML(ahtml, interestingClasses, false);
    }

    /** Translates the ahtml with simple rule guards into remopla.
     *  @param ahtml AbstractHTML object representing ahtml to translate
     *  @param interestingClasses css classes that might not be pertinent to the
     *  execution, but we still want to know if they're added or not
     *  @param interestingXVars whether to add X variables for the interesting
     *  classes
     *  @return the Remopla object corresponding to the css -- the main
     *  function will be the entry point
     */
    public RemoplaAndVars
        translateSimpleHTML(AbstractHTML<SimpleRuleGuard> ahtml,
                            Collection<CSSClass> interestingClasses,
                            boolean interestingXVars)
    throws Exception {
        Collection<Rule<SimpleRuleGuard>> rules
            = getPertinentRules(ahtml, interestingClasses);

        TranVars vars = getTranVars(rules,
                                    interestingClasses,
                                    interestingXVars);

        Map<String, Rule<SimpleRuleGuard>> ruleMap
            = new HashMap<String, Rule<SimpleRuleGuard>>();

        return new RemoplaAndVars(makeRemopla(rules,
                                              vars,
                                              interestingClasses,
                                              ruleMap),
                                  vars,
                                  ruleMap);
    }



    ////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    //

    private TranVars getTranVars(Collection<Rule<SimpleRuleGuard>> rules,
                                 Collection<CSSClass> interestingClasses,
                                 boolean interestingXVars) {
        TranVars tvs = new TranVars(rules,
                                    interestingClasses,
                                    interestingXVars);
        return tvs;
    }

    private RemoplaAndOptimisations makeRemopla(
                Collection<Rule<SimpleRuleGuard>> rules,
                TranVars vars,
                Collection<CSSClass> interestingClasses,
                Map<String, Rule<SimpleRuleGuard>> ruleMap
            ) throws Exception {
        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations
            = new HashMap<RemoplaStmt, StmtOptimisations>();
        Remopla remopla = new Remopla();

        addGlobalVars(remopla, vars);
        addNodeMethod(remopla, rules, vars, interestingClasses, ruleMap);

        return new RemoplaAndOptimisations(remopla,
                                           stmtOptimisations);
    }

    private void addGlobalVars(Remopla remopla,
                               TranVars vars) throws Exception {
        remopla.addGlobalVar(vars.getPopVar());
        for (CSSClass c : vars.getXVarClasses()) {
            remopla.addGlobalVar(vars.getXVar(c));
        }
    }

    /**
     * @param remopla the remopla being built
     * @param rules the pertinent ahtml rules being built from
     * @param vars the vars to use
     * @param interestingClasses the classes we're interested in adding
     * @param ruleMap maps labels of the remopla to the rules they came from
     * (added to by method)
     */
    private void addNodeMethod(Remopla remopla,
                               Collection<Rule<SimpleRuleGuard>> rules,
                               TranVars vars,
                               Collection<CSSClass> interestingClasses,
                               Map<String, Rule<SimpleRuleGuard>> ruleMap)
    throws Exception {
        // could do with splitting, but meh

        // create method definition
        List<RemoplaVar> parameters = new ArrayList<RemoplaVar>();

        parameters.add(vars.getRootVar());
        for (CSSClass c : vars.getYVarClasses()) {
            parameters.add(vars.getYVar(c));
        }
        for (CSSClass c : vars.getZVarClasses()) {
            parameters.add(vars.getZVar(c));
        }

        RemoplaMethod method = new RemoplaMethod(RemoplaMethod.ReturnType.VOID,
                                                 Namer.getNodeMethodName(),
                                                 parameters);

        // create method body loop
        RemoplaDoStmt loop = new RemoplaDoStmt();
        RuleToMoped r2m = new RuleToMoped();

        // parts of loop for rules
        for (Rule<SimpleRuleGuard> r : rules) {
            r2m.translateRule(r, vars);

            RemoplaExpr guard = r2m.getGuard();
            List<RemoplaStmt> stmts = r2m.getStmts();

            if (guard != null)
                loop.addClause(guard, stmts);

            if (!stmts.isEmpty()) {
                String label = getNewStmtLabel();
                stmts.get(0).setLabel(label);
                ruleMap.put(label, r);
            }
        }

        // part of loop for returning
        List<RemoplaStmt> ret = new ArrayList<RemoplaStmt>();

        RemoplaAssignStmt assign = new RemoplaAssignStmt();
        for (CSSClass c : vars.getXVarClasses()) {
            if (vars.hasYVar(c))
                assign.addParallelAssignment(vars.getXVar(c), vars.getYVar(c));
            else
                assign.addParallelAssignment(vars.getXVar(c), rbool(false));
        }
        assign.addParallelAssignment(vars.getPopVar(), rbool(true));

        ret.add(assign);
        ret.add(rreturn());
        loop.addClause(not(vars.getRootVar()), ret);

        method.appendStatement(loop);
        remopla.addMethod(method);
    }


    /**
     * get label names for statements to match with rules
     */
    private static int nextStmtLabelIdx = 0;
    private String getNewStmtLabel() {
        return Namer.getRuleStatementLabel(nextStmtLabelIdx++);
    }


    /**
     * @param ahtml the html to get the rules from
     * @param interestingClasses the classes we're interested in adding
     * @return the set of rules of the ahtml that might eventually lead to an
     * interesting class being added
     */
    private Collection<Rule<SimpleRuleGuard>>
    getPertinentRules(AbstractHTML<SimpleRuleGuard> ahtml,
                      Collection<CSSClass> interestingClasses) {
        Map<CSSClass, Collection<Rule<SimpleRuleGuard>>> classToRules
            = getClassToRulesMap(ahtml);

        Set<Rule<SimpleRuleGuard>> pertinentRules
          = new HashSet<Rule<SimpleRuleGuard>>();
        Set<CSSClass> worklist
          = new HashSet<CSSClass>(interestingClasses);
        Set<CSSClass> done = new HashSet<CSSClass>();

        boolean addedAddChildRules = false;

        while (!worklist.isEmpty()) {
            CSSClass c = worklist.iterator().next();
            worklist.remove(c);
            done.add(c);

            Collection<Rule<SimpleRuleGuard>> crules
                = classToRules.get(c);

            if (crules != null) {
                for (Rule<SimpleRuleGuard> r : crules) {
                    pertinentRules.add(r);

                    SimpleRuleGuard g = r.getGuard();

                    // if we have an up guard we need the add child rules
                    if (!addedAddChildRules && !g.getUpClasses().isEmpty()) {
                        addedAddChildRules = true;
                        addAddChildRules(ahtml, pertinentRules, worklist, done);
                    }

                    addGuardToPertWorklist(g, worklist, done);
                }
            }

        }

        return pertinentRules;
    }


    /**
     * @param ahtml the ahtml to get the rules from
     * @return a map from classes to the rules with the class on the rhs
     */
    private Map<CSSClass, Collection<Rule<SimpleRuleGuard>>>
    getClassToRulesMap(AbstractHTML<SimpleRuleGuard> ahtml) {
        Map<CSSClass, Collection<Rule<SimpleRuleGuard>>> map
            = new HashMap<CSSClass, Collection<Rule<SimpleRuleGuard>>>();

        for (Rule<SimpleRuleGuard> r : ahtml.getRules()) {
            for (CSSClass c : r.getRhsClasses()) {
                Collection<Rule<SimpleRuleGuard>> rs = map.get(c);
                if (rs == null) {
                    rs = new HashSet<Rule<SimpleRuleGuard>>();
                    map.put(c, rs);
                }
                rs.add(r);
            }
        }

        return map;
    }


    /**
     * @param g the guard of the rule being set as pertinent
     * @param worklist the list of css classes to process to find pert rules
     * @param done the list of css classes already looked at
     */
    private void addGuardToPertWorklist(SimpleRuleGuard g,
                                        Collection<CSSClass> worklist,
                                        Collection<CSSClass> done) {
        for (CSSClass upc : g.getUpClasses()) {
            if (!done.contains(upc))
                worklist.add(upc);
        }
        for (CSSClass flatc : g.getFlatClasses()) {
            if (!done.contains(flatc)) {
                worklist.add(flatc);
            }
        }
        for (CSSClass downc : g.getDownClasses()) {
            if (!done.contains(downc))
                worklist.add(downc);
        }
    }


    /**
     * @param ahtml the ahtml to get the rules from
     * @param pertinentRules the set of pertinent rules to add to
     * @param worklist the worklist of css classes to process (adds new classes)
     * @param done the list of already done classes
     */
    private void
    addAddChildRules(AbstractHTML<SimpleRuleGuard> ahtml,
                     final Collection<Rule<SimpleRuleGuard>> pertinentRules,
                     final Collection<CSSClass> worklist,
                     final Collection<CSSClass> done) {
        final HTMLToMopedTranslator self = this;

        for (final Rule<SimpleRuleGuard> r : ahtml.getRules()) {
            r.accept(new RuleVisitor<SimpleRuleGuard>() {
                public void visit(RuleAddClass<SimpleRuleGuard> g) {
                    // do nothing
                }

                public void visit(RuleAddChild<SimpleRuleGuard> g) {
                    pertinentRules.add(r);
                    self.addGuardToPertWorklist(r.getGuard(),
                                                worklist,
                                                done);
                }

            });
        }
    }

}
