/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.hacks;

import java.util.ArrayList;
import java.util.List;

import treeped.main.CmdOptions;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleSheet;

import com.steadystate.css.dom.AbstractCSSRuleImpl;
import com.steadystate.css.dom.CSSRuleListImpl;


/**
 * Because cssparser doesn't properly support equality of css rules (i.e. same
 * file, same location) we're going to hack it in by adding a wrapper of all the
 * css rules that does consider to identically located files to be equal
 */
public class CSSRuleWrapper implements CSSRule {

    private CSSRule rule;

    /**
     * @param rule the rule to wrap, assumed to be AbstractCSSRuleImpl
     */
    public CSSRuleWrapper(CSSRule rule) {
        this.rule = rule;
    }

    public CSSRule getParentRule() {
       return rule.getParentRule();
    }

    public CSSStyleSheet getParentStyleSheet() {
        return rule.getParentStyleSheet();
    }

    public void setCssText(String s) {
        rule.setCssText(s);
    }

    public String getCssText() {
        return rule.getCssText();
    }

    public short getType() {
        return rule.getType();
    }

    /**
     * Equal if they have the same user data, that is, the same location (since
     * that's what cssparser stores as user data.  there's a reason this package
     * is called hacks...)
     */
    public boolean equals(Object o) {
        if (o instanceof CSSRuleWrapper) {
            CSSRuleWrapper w = (CSSRuleWrapper)o;
            AbstractCSSRuleImpl arule = ((AbstractCSSRuleImpl)rule);
            AbstractCSSRuleImpl orule = (AbstractCSSRuleImpl)w.rule;
            return (arule.getUserDataMap().equals(orule.getUserDataMap()));
        }
        return false;
    }

    /**
     * Returns the userdata map hashcode for the cssparser implementation of
     * cssrules (rather than the hashcode for the rule which does not consider
     * two rules taken from the same file and location to be the same)
     */
    public int hashCode() {
        return ((AbstractCSSRuleImpl)rule).getUserDataMap().hashCode();
    }


    public String toString() {
        String s = rule.toString();
        if (CmdOptions.getOutputCssRuleInfo())
            s += "\n  [" + getUserDataString() + "]";
        return s;
    }

    /**
     * The user data attached to the rule as a string (contains location info)
     */
    public String getUserDataString() {
        return ((AbstractCSSRuleImpl)rule).getUserDataMap().toString();

    }
}
