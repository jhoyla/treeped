/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* Main class for Jimple To Moped conversion
 * Written by: Matthew Hague
 *
 * Initially based on the fooanalasys example from
 *
 *		A Survivor's Guide to Java Program Analysis with Soot
 *		A. Einarsson and J. D. Nielsen
 */


package treeped.main;

import treeped.css.analysis.HTMLAnalyser;
import treeped.css.analysis.RealHTMLAnalyser;
import treeped.css.representation.CSSClass;

import org.apache.log4j.Logger;

public class Main {

    static Logger logger = Logger.getLogger(Main.class);


	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) {
        try {
            if (doProcessCommandLine(args)) {
                if (CmdOptions.getRealHTML())
                    doRealHTMLAnalysis();
                else if (CmdOptions.getAHTML())
                    doAHTMLAnalysis();
            } else {
                logger.error(CmdOptions.usage());
            }
        } catch (Exception e) {
            logger.error(e);
        }
	}


    ///////////////////////////////////////////////////////////////
    // Private Methods
    //

    private static void doRealHTMLAnalysis() {
        RealHTMLAnalyser.analyseRealHTML(CmdOptions.getHTMLFiles());
    }

    private static void doAHTMLAnalysis() {
        try {
            CSSClass witnessClass = null;
            String witnessClassName = CmdOptions.getWitnessClass();
            if (witnessClassName != null)
                witnessClass = new CSSClass(witnessClassName);
            HTMLAnalyser a = new HTMLAnalyser();
            HTMLAnalyser.AnalysisResult res
                = a.analyseHTMLFile(CmdOptions.getAHTMLFile(),
                                    CmdOptions.getFullTree(),
                                    witnessClass);
            System.out.println("Result:");
            System.out.println(res);
            if (witnessClass != null) {
                System.out.println("Witness:");
                System.out.println(res.getWitness());
            }
        } catch (Exception e) {
            System.err.println("Error analysing " + CmdOptions.getAHTMLFile());
            System.err.println(e);
            e.printStackTrace();
        }
    }


    /** Given the command line (broken up a-la java), sets up CmdOptions,
     * returns false if an error occured
     */
    public static boolean doProcessCommandLine(String[] args) {
        return CmdOptions.readCommandLine(args);
    }
}




