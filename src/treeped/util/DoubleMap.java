/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Double map taken from cshore, repeated unchanged below, with license, bar a
 * change of package name
 */

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2012-2014 Christopher Broadbent, Arnaud Carayol, Matthew Hague, Olivier Serre
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */




package treeped.util;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;


/**
 * Implementation of DoubleMap using a nesting of hash maps.
 *
 * DoubleMapTest seems to suggest this is the more reliable doublemap.
 */
public class DoubleMap<K1, K2, V> {

    Map<K1, Map<K2, V>> map = new HashMap<K1, Map<K2, V>>();

    private Set<K2> emptyK2Set = new HashSet<K2>();

    public DoubleMap() { }

    public V get(K1 k1, K2 k2) {
        Map<K2, V> map2 = map.get(k1);
        if (map2 != null) {
            return map2.get(k2);
        }
        return null;
    }

    public V put(K1 k1, K2 k2, V v) {
        Map<K2, V> map2 = map.get(k1);
        if (map2 != null) {
            return map2.put(k2, v);
        } else {
            map2 = new HashMap<K2, V>();
            map2.put(k2, v);
            map.put(k1, map2);
            return null;
        }
    }

    public Collection<V> values() {
        Collection<V> values = new HashSet<V>();
        Collection<Map<K2, V>> innerMaps = map.values();
        for (Map<K2, V> map2 : innerMaps) {
            values.addAll(map2.values());
        }
        return values;
    }

    /**
     * @param k1 the K1 key
     * @return null, or the set of values associated with key k1
     */
    public Collection<V> values(K1 k1) {
        Map<K2, V> innerMap = map.get(k1);
        if (innerMap != null)
            return innerMap.values();
        else
            return null;
    }


    public int size() {
        int size = 0;
        Collection<Map<K2, V>> innerMaps = map.values();
        for (Map<K2, V> map2 : innerMaps) {
            size += map2.size();
        }
        return size;
    }

    public void clear() {
        map.clear();
    }


    public Collection<K2> keySet(K1 k1) {
        Map<K2, V> map2 = map.get(k1);
        if (map2 == null)
            return emptyK2Set;
        else
            return map2.keySet();
    }

    /**
     * @return the set of K1 keys
     */
    public Collection<K1> keySet() {
        return map.keySet();
    }

}
