/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Map from K to Collection<V>
 */
public class MultiMap<K, V> {

    private Map<K, Collection<V>> map = new HashMap<K, Collection<V>>();

    public MultiMap() { }

    /**
     * adds v to collection of elements mapped to k
     *
     * @param k the key
     * @param v the new value
     */
    public void put(K k, V v) {
        Collection<V> vs = map.get(k);
        if (vs == null) {
            vs = new HashSet<V>();
            map.put(k, vs);
        }
        vs.add(v);
    }

    /**
     * adds new vs to collection of elements mapped to k
     *
     * @param k the key
     * @param newVs the new values
     */
    public void putAll(K k, Collection<V> newVs) {
        Collection<V> vs = map.get(k);
        if (vs == null) {
            vs = new HashSet<V>();
            map.put(k, vs);
        }
        vs.addAll(newVs);
    }


    /**
     * @param k
     * @return collection of elements mapped to k
     */
    public Collection<V> get(K k) {
        Collection<V> vs = map.get(k);
        return (vs == null) ? null
                            : Collections.unmodifiableCollection(vs);
    }

    /**
     * @return the set of keys with mappings
     */
    public Collection<K> keySet() {
        return map.keySet();
    }


    public Collection<Map.Entry<K, Collection<V>>> entrySet() {
        return map.entrySet();
    }


    /**
     * @return a collection of all Vs appearing in a collection on the rhs of
     * the map
     */
    public Collection<V> individualValues() {
        Collection<V> all = new HashSet<V>();
        for (Collection<V> vs : map.values())
            all.addAll(vs);
        return all;
    }

    /**
     * Inverts map so if v was mapped to k, k now mapped to v
     */
    public MultiMap<V, K> inverseMap() {
        MultiMap<V, K> inverse = new MultiMap<V, K>();
        for (Map.Entry<K, Collection<V>> e : map.entrySet()) {
            K k = e.getKey();
            for (V v : e.getValue()) {
                inverse.put(v, k);
            }
        }
        return inverse;
    }


    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    public StringBuffer toString(StringBuffer sb) {
        for (Map.Entry<K, Collection<V>> e : map.entrySet()) {
            sb.append(e.getKey());
            sb.append(" : ");
            sb.append(e.getValue());
            sb.append("\n");
        }
        return sb;
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }
}
