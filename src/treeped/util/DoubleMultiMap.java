/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Map from K1, K2 to Collection<V>
 */
public class DoubleMultiMap<K1, K2, V> {

    private DoubleMap<K1, K2, Collection<V>> map
        = new DoubleMap<K1, K2, Collection<V>>();

    public DoubleMultiMap() { }

    /**
     * adds v to collection of elements mapped to k1, k2
     *
     * @param k1 the key
     * @param k2 the second key
     * @param v the new value
     */
    public void put(K1 k1, K2 k2, V v) {
        Collection<V> vs = map.get(k1, k2);
        if (vs == null) {
            vs = new HashSet<V>();
            map.put(k1, k2, vs);
        }
        vs.add(v);
    }

    /**
     * adds vs to collection of elements mapped to k1, k2
     *
     * @param k1 the key
     * @param k2 the second key
     * @param newVs the new values
     */
    public void putAll(K1 k1, K2 k2, Collection<V> newVs) {
        Collection<V> vs = map.get(k1, k2);
        if (vs == null) {
            vs = new HashSet<V>();
            map.put(k1, k2, vs);
        }
        vs.addAll(newVs);
    }

    /**
     * @param k1 first key
     * @param k2 second key
     * @return collection of elements mapped to k1, k2
     */
    public Collection<V> get(K1 k1, K2 k2) {
        Collection<V> vs = map.get(k1, k2);
        return (vs == null) ? null
                            : Collections.unmodifiableCollection(vs);
    }



    /**
     * @param k1 a key
     * @return collection of k2s associated
     */
    public Collection<K2> keySet(K1 k1) {
        return map.keySet(k1);
    }

    /**
     * @return the set of K1 keys
     */
    public Collection<K1> keySet() {
        return map.keySet();
    }


}
