/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// StmtToRule.java
// Author: Matthew Hague
//
// A class for converting RemoplaStmt objects to Pds rules.  See
// RemoplaToPDS.java for the translation specifics.


// TODO: Refactor the handling of semiRingGuard or clarify it's use.  All a bit
// global var at the moment.

package treeped.remoplatopds;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.tum.in.wpds.Rule;
import treeped.representation.*;
import treeped.representation.RemoplaStmt;
import org.apache.log4j.Logger;
import static treeped.representation.RemoplaStaticFactory.*;

public class StmtToRules extends AbstractRemoplaStmtVisitor {

    static Logger logger = Logger.getLogger(StmtToRules.class);

    private static RemoplaExprNormaliser normaliser = new RemoplaExprNormaliser();

    Remopla currentRemopla;
    Map<RemoplaStmt, StmtOptimisations> stmtOptimisations;

    Collection<Rule> result = null;
    String nextStmtLabel = null;
    String breakLabel = null;

    private RemoplaExpr semiRingGuard = null;

    /** Translates a list of RemoplaStmt into a collection of pushdown rules
     * labelled with an ExprSemiring object
     *   @param stmts the list of statements to translate.  Adds a default label
     *   to the statement if it doesn't have one.
     *   @param nextStmt the statement to be executed next (provided stmt doesn't
     *   jump, &c.).  Null if there is no next statement (assumed to return from
     *   current method).  Adds a label to the statement if it does not have
     *   one.
     *   @param currentRemopla the current remopla instance
     *   @param stmtOptimisations map of optimisations for the remopla statements
     *   @param firstStatementGuard a conditional -- the first statement will
     *   only be executed if this guard is satisfied
     */
    public Collection<Rule> translateStmtBlock(List<RemoplaStmt> stmts,
                                               RemoplaStmt nextStmt,
                                               Remopla currentRemopla,
                                               Map<RemoplaStmt, StmtOptimisations> stmtOptimisations,
                                               RemoplaExpr firstStatementGuard) {
        assert currentRemopla != null && stmtOptimisations != null;
        this.currentRemopla = currentRemopla;
        this.stmtOptimisations = stmtOptimisations;

        Collection<Rule> or = result;
        result = new HashSet<Rule>(5);

        subTranslateStmtBlock(stmts, getAddLabel(nextStmt), firstStatementGuard);

        Collection<Rule> result = this.result;
        this.result = or;

        return result;
    }

    public Collection<Rule> translateStmtBlock(List<RemoplaStmt> stmts,
                                               RemoplaStmt nextStmt,
                                               Remopla currentRemopla,
                                               Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        return translateStmtBlock(stmts, nextStmt, currentRemopla, stmtOptimisations, null);
    }

    /** Translates a given RemoplaStmt into a collection of pushdown rules
     * labelled with an ExprSemiring object
     *   @param stmt the statement to translate, non-null.
     *   Adds a label to the statement if it does not have one.
     *   @param nextStmt the statement to be executed next (provided stmt doesn't
     *   jump, &c.).  Null if there is no next statement (assumed to return from
     *   current method).  Adds a label to the statement if it does not have
     *   one.
     *   @param currentRemopla the current Remopla instance
     *   @param stmtOptimisations map of optimisations for the remopla statements
     *   @param firstStatementGuard a conditional -- the first statement will
     *   only be executed if this guard is satisfied
     */
    public Collection<Rule> translateStmt(RemoplaStmt stmt,
                                          RemoplaStmt nextStmt,
                                          Remopla currentRemopla,
                                          Map<RemoplaStmt, StmtOptimisations> stmtOptimisations,
                                          RemoplaExpr firstStatementGuard) {
        assert currentRemopla != null && stmt != null && stmtOptimisations != null;
        this.currentRemopla = currentRemopla;
        this.stmtOptimisations = stmtOptimisations;

        Collection<Rule> or = result;
        result = new HashSet<Rule>(5);

        subTranslateStmt(stmt, getAddLabel(nextStmt), firstStatementGuard);

        Collection<Rule> result = this.result;
        this.result = or;

        return result;
    }


    public Collection<Rule> translateStmt(RemoplaStmt stmt,
                                          RemoplaStmt nextStmt,
                                          Remopla currentRemopla,
                                          Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        return translateStmt(stmt, nextStmt, currentRemopla, stmtOptimisations, null);
    }

    public void visit(RemoplaAssignStmt as) {
        // method calls must be handled differently because of return values
        // TODO: Refactor Remopla to prevent illegal usage (Remopla doesn't
        // allow method calls in the middle of expressions)
        makePlainRule(as, getAddLabel(as), nextStmtLabel);
    }

    public void visit(RemoplaInvokeAssignStmt ias) {
        String returnHandler = Namer.getNewReturnHandler();
        RemoplaMethodCall mc = ias.getRight();
        RemoplaMethod m = currentRemopla.getMethod(mc.getCallString());
        RemoplaInvokeStmt invoke = new RemoplaInvokeStmt(ias.getRight());
        makePushRule(invoke, getAddLabel(ias), m, returnHandler);

        // assign return value (passing an expression rather than a Stmt results in a return
        // handler ExprSemiring)
        makePlainRule(ias.getLeft(), returnHandler, nextStmtLabel);
    }


    public void visit(RemoplaBreakStmt bs) {
        // breakLabel will be set by the enclosing do stmt
        makePlainRule(bs, getAddLabel(bs), breakLabel);
    }


    public void visit(RemoplaDoStmt ds) {
        String obl = this.breakLabel;
        this.breakLabel = nextStmtLabel;

        translateGuardedStmt(ds, getAddLabel(ds));

        this.breakLabel = obl;
    }



    public void visit(RemoplaGotoStmt gs) {
        makePlainRule(gs, getAddLabel(gs), gs.getTarget());
    }




    public void visit(RemoplaIfStmt is) {
        translateGuardedStmt(is, nextStmtLabel);
    }



    public void visit(RemoplaInvokeStmt is) {
        RemoplaMethodCall mc = is.getInvokeExpr();
        RemoplaMethod m = currentRemopla.getMethod(mc.getCallString());
        makePushRule(is, getAddLabel(is), m, nextStmtLabel);
    }


    public void visit(RemoplaNullStmt obj) {
        // Null: do nothing
    }


    public void visit(RemoplaReturnStmt rs) {
        makePopRule(rs, getAddLabel(rs));
    }


    public void visit(RemoplaSkipStmt ss) {
        makePlainRule(ss, getAddLabel(ss), nextStmtLabel);
    }


    public void defaultCase(RemoplaStmt stmt) {
        logger.error("Unhandled Remopla structure in StmtToRules: " + stmt);
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    //

    /** String stmtLabel is the label of the current top stack **/
    private void makePopRule(RemoplaStmt stmt, String stmtLabel) {
        ExprSemiring expr = new ExprSemiring(stmt, ExprSemiring.RuleType.POP);
        makePopRule(expr, stmtLabel);
    }


    /** String stmtLabel is the label of the current top stack **/
    private void makePopRule(ExprSemiring expr, String stmtLabel) {
        String q = Namer.getControlState();
        expr.setCondition(semiRingGuard);
        result.add(new Rule(expr, q, stmtLabel, q));
    }


    /** Assumes that if the next statement label is null, we're doing a pop with
     * no return value
     */
    private void makePlainRule(RemoplaStmt stmt, String stmtLabel, String nextLabel) {
        ExprSemiring expr = new ExprSemiring(stmt, ExprSemiring.RuleType.REWRITE);
        makePlainRule(expr, stmtLabel, nextLabel);
    }

    private void makePlainRule(RemoplaExpr e, String stmtLabel, String nextLabel) {
        ExprSemiring expr = new ExprSemiring(e);
        makePlainRule(expr, stmtLabel, nextLabel);
    }

    /** Assumes that if the next statement label is null, we're doing a pop with
     * no return value
     */
    private void makePlainRule(ExprSemiring expr, String stmtLabel, String nextLabel) {
        expr.setCondition(semiRingGuard);
        if (nextLabel != null) {
            String q = Namer.getControlState();
            result.add(new Rule(expr, q, stmtLabel, q, nextLabel));
        } else {
            makePopRule(expr, stmtLabel);
        }
    }

    /** If there is no next label, we're really doing a rewrite (since the
     * return would go to a state that would return.  That is for a stack "a w" we
     * normally push to "b a' w" where a' is the return site.  But, if a' is null we
     * just have "b w".
     *
     *  If the method has no statements, just a plain rule to the return site is
     *  created
     *
     *   @param stmt the remopla statement on the rule
     *   @param stmtLabel the statement calling
     *   @param m the method to be called
     *   @param nextLabel the return site
     */
    private void makePushRule(RemoplaStmt stmt, String stmtLabel, RemoplaMethod m, String nextLabel) {
        // We're going to the first statement in the method
        List<RemoplaStmt> methodStmts = m.getStmts();
        if (methodStmts.size() == 0) {
            makePlainRule(stmt, stmtLabel, nextLabel);
        } else {
            String methodLabel = getAddLabel(methodStmts.get(0));
            if (nextLabel == null) {
                makePlainRule(stmt, stmtLabel, methodLabel);
            } else {
                ExprSemiring expr = new ExprSemiring(stmt, ExprSemiring.RuleType.PUSH);
                expr.setCondition(semiRingGuard);
                String q = Namer.getControlState();
                result.add(new Rule(expr, q, stmtLabel, q, methodLabel, nextLabel));
             }
        }
    }




    /** like translateStmtBlock, but does not new or free the result set, just
     * adds to it, and takes a string next statement label rather than a
     * statement
     */
    private void subTranslateStmtBlock(List<RemoplaStmt> stmts, String nextStmtLabel, RemoplaExpr firstStatementGuard) {
        assert stmts != null;

        String onsl = this.nextStmtLabel;

        String blockExitLabel = nextStmtLabel;

        Iterator i = stmts.iterator();
        RemoplaStmt blockNext = i.hasNext() ? (RemoplaStmt)i.next() : null;
        while (blockNext != null) {
            RemoplaStmt stmt = blockNext;
            blockNext = i.hasNext() ? (RemoplaStmt)i.next() : null;

            this.nextStmtLabel = blockNext != null ? getAddLabel(blockNext) : blockExitLabel;
            this.semiRingGuard = firstStatementGuard;

            stmt.accept(this);

            firstStatementGuard = null;
            this.semiRingGuard = null;
        }

        this.nextStmtLabel = onsl;
    }


    /** like translateStmt, but does not free or create the result set, just
     * adds to it, but takes a string next statement label rather than a stmt
     */
    private void subTranslateStmt(RemoplaStmt stmt, String nextStmtLabel, RemoplaExpr firstStatementGuard) {
        assert stmt != null;

        String onsl = this.nextStmtLabel;

        this.nextStmtLabel = nextStmtLabel;

        this.semiRingGuard = firstStatementGuard;

        stmt.accept(this);

        this.nextStmtLabel = onsl;
    }

    private String getAddLabel(RemoplaStmt s) {
        String l;
        if (s == null) {
            l = null;
        } else {
            l = s.getLabel();
            if (l == null) {
                l = Namer.getNewLabel();
                s.setLabel(l);
            }
        }
        return l;
    }


    private void translateGuardedStmt(RemoplaGuardedStmt gs, String continueLabel) {
        RemoplaExpr elseGuard = null;

        int i = 0;
        for (RemoplaGuardedStmt.GuardStmt g : gs.getClauses()) {
            RemoplaExpr condition = g.getCondition();
            List<RemoplaStmt> stmts = g.getStmts();

            if (gs.hasElse()) {
                if (elseGuard == null)
                    elseGuard = condition;
                else
                    elseGuard = or(elseGuard, condition);
            }


            if (isSensitiveClause(gs, g)) {
                RemoplaSkipStmt guard = new RemoplaSkipStmt(condition);
                makePlainRule(guard, getAddLabel(gs), getAddLabel(stmts.get(0)));
                subTranslateStmtBlock(stmts, continueLabel, null);
            } else {
                RemoplaStmt firstStmt = stmts.get(0);
                String firstLab = firstStmt.getLabel();
                firstStmt.setLabel(getAddLabel(gs));
                subTranslateStmtBlock(stmts, continueLabel, condition);
                firstStmt.setLabel(firstLab);
            }
        }

        if (gs.hasElse()) {
            if (elseGuard != null) {
                elseGuard = normaliser.normaliseExpression(not(elseGuard));
            } else {
                elseGuard = rbool(true);
            }

            List<RemoplaStmt> elseClause = gs.getElse();

            if (hasSensitiveElse(gs)) {
                RemoplaSkipStmt guard = new RemoplaSkipStmt(elseGuard);
                makePlainRule(guard, getAddLabel(gs), getAddLabel(elseClause.get(0)));
                subTranslateStmtBlock(elseClause, continueLabel, null);
            } else {
                RemoplaStmt firstStmt = elseClause.get(0);
                String firstLab = firstStmt.getLabel();
                firstStmt.setLabel(getAddLabel(gs));
                subTranslateStmtBlock(elseClause, continueLabel, elseGuard);
                firstStmt.setLabel(firstLab);
            }
        }
    }


    private boolean isSensitiveClause(RemoplaGuardedStmt gs, RemoplaGuardedStmt.GuardStmt g) {
        boolean is = true;
        StmtOptimisations opts = stmtOptimisations.get(gs);
        if (opts != null) {
            is = opts.sensitiveToIntermediateLabels(g);
        }
        return is;
    }

    private boolean hasSensitiveElse(RemoplaGuardedStmt gs) {
        boolean is = true;
        StmtOptimisations opts = stmtOptimisations.get(gs);
        if (opts != null) {
            is = opts.getElseSensitiveToIntermediateLabels();
        }
        return is;
    }
}
