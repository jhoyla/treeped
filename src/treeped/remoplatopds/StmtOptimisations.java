/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// StmtOptimisations.java
// Author: Matthew Hague
//
// A class containing optimisation info for a particular statement


package treeped.remoplatopds;

import java.util.Collection;
import java.util.HashSet;

import treeped.representation.RemoplaAssignStmt;
import treeped.representation.RemoplaGuardedStmt;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaVar;

import org.apache.log4j.Logger;

public class StmtOptimisations {
    static Logger logger = Logger.getLogger(StmtOptimisations.class);


    private RemoplaStmt stmt;
    private boolean dangerousAssign = false;
    private Collection<RemoplaGuardedStmt.GuardStmt> insensitiveToIntermediateLabels = null;
    private boolean elseSensitiveToIntermediateLabels = true;
    private Collection<RemoplaVar> dyingVars = null;


    public StmtOptimisations(RemoplaStmt stmt) {
        assert stmt != null;
        this.stmt = stmt;
    }

    public void setDangerousAssign(boolean dangerousAssign) {
        this.dangerousAssign = dangerousAssign;
    }

    public boolean canDangerousAssign() {
        assert (stmt instanceof RemoplaAssignStmt);
        return (dangerousAssign || ((RemoplaAssignStmt)stmt).getAssignments().size() <= 1);
    }

    public void addInsensitiveGuard(RemoplaGuardedStmt.GuardStmt gs) {
        if (insensitiveToIntermediateLabels == null) {
            insensitiveToIntermediateLabels = new HashSet<RemoplaGuardedStmt.GuardStmt>();
        }
        insensitiveToIntermediateLabels.add(gs);
    }

    public boolean sensitiveToIntermediateLabels(RemoplaGuardedStmt.GuardStmt gs) {
        return insensitiveToIntermediateLabels == null || !insensitiveToIntermediateLabels.contains(gs);
    }

    public void setElseSensitiveToIntermediateLabels(boolean elseSensitiveToIntermediateLabels) {
        this.elseSensitiveToIntermediateLabels = elseSensitiveToIntermediateLabels;
    }

    public boolean getElseSensitiveToIntermediateLabels() {
        return elseSensitiveToIntermediateLabels;
    }


    public String toString() {
        String s = "StmtOptimisations [" + stmt + "]\n";
        s += "  dangerousAssign = " + dangerousAssign + "\n";
        if (insensitiveToIntermediateLabels != null) {
            for (RemoplaGuardedStmt.GuardStmt g : insensitiveToIntermediateLabels) {
                s += "  " + g + " is insensitive.\n";
            }
        }
        s += "  else is sensitive: " + elseSensitiveToIntermediateLabels + "\n";
        return s;
    }

    public void addDyingVars(Collection<RemoplaVar> dyingVars) {
        assert dyingVars != null;
        this.dyingVars = dyingVars;
    }


    public Collection<RemoplaVar> getDyingVars() {
        return dyingVars;
    }

    public boolean hasDyingVarsInfo() {
        return (dyingVars != null);
    }
}
