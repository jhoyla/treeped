/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Variable.java
// Author: Matthew Hague
//
// Contains information on variables for use by VarManager
//
// A Variable contains information and possible sub variables
// (Eg, an array has a sub variable for each element)
// Note: variables do not have a RemoplaVar and index if they have a list of
// subvariables.  And vice versa.


package treeped.remoplatopds;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import treeped.representation.AbstractRemoplaVarVisitor;
import treeped.representation.RemoplaArrayVar;
import treeped.representation.RemoplaBoolVar;
import treeped.representation.RemoplaEnumVar;
import treeped.representation.RemoplaIntConstVar;
import treeped.representation.RemoplaIntVar;
import treeped.representation.RemoplaStructVar;
import treeped.representation.RemoplaUnsupportedVar;
import treeped.representation.RemoplaVar;


import org.apache.log4j.Logger;



public class Variable extends AbstractRemoplaVarVisitor {


    static Logger logger = Logger.getLogger(Variable.class);

    private RemoplaVar rvar; // a variable should not have an rvar/index
    private int index = -1; // and subvariables
    private int savedIndex = -1;
    private int bits; // the total number of bits required (include subvars)
    private int totalVars; // the total number of variables including subvars (nested)

    List<Variable> subVars;

	public Variable(RemoplaVar rvar, int defaultBits) {
        assert rvar != null;
        this.rvar = rvar;
        bits = defaultBits;
        rvar.accept(this);
	}

	/**
	 * Returns the name of the variable.
	 *
	 * @return the name of the variable.
	 */
	public String getName() {
		return rvar.getName();
	}

	public int getBits() {
        return bits;
	}

    public int getTotalVars() {
        return totalVars;
    }

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}


	public int getSavedIndex() {
		return savedIndex;
	}

	public void setSavedIndex(int savedIndex) {
		this.savedIndex = savedIndex;
	}

    public RemoplaVar getRemoplaVar() {
        return rvar;
    }

    public List<Variable> getSubVars() {
        return subVars;
    }

    public List<Variable> flattenSubVars() {
        List<Variable> flat = null;
        if (subVars != null) {
            flat = new ArrayList<Variable>();
            for (Variable v : subVars) {
                if (v.getSubVars() != null) {
                    flat.addAll(v.flattenSubVars());
                } else {
                    flat.add(v);
                }
            }
        }
        return flat;
    }

	public String toString() {
        return rvar.toString();
	}




    public void visit(RemoplaIntVar ivar) {
        bits = ivar.getBits() == -1 ? bits : ivar.getBits();
        subVars = null;
        totalVars = 1;
    }

    public void visit(RemoplaIntConstVar icvar) {
        logger.error("Variable.visit(RemoplaIntConstVar) not implemented!");
    }

    public void visit(RemoplaBoolVar bvar) {
        bits = 1;
        subVars = null;
        totalVars = 1;
    }

    // enums are just having the default number of bits for the time being
    public void visit(RemoplaEnumVar evar) {
        bits = bits; // bitsRequired(evar.getType().getValues().size());
        subVars = null;
        totalVars = 1;
    }

    public void visit(RemoplaArrayVar avar) {
        int defaultBits = bits;
        bits = 0;
        totalVars = 0;
        if (avar.getDimension() == 1) {
            subVars = new ArrayList<Variable>(avar.getMaxIndex() - avar.getMinIndex() + 1);
            for (int i = avar.getMinIndex(); i <= avar.getMaxIndex(); i++) {
                Variable sv = new Variable(avar.getType(), defaultBits);
                bits += sv.getBits();
                totalVars += sv.getTotalVars();
                subVars.add(sv);
            }
        } else { // dimensions = 2
            int rows = avar.getMaxIndex() - avar.getMinIndex() + 1;
            int cols = avar.getMaxIndex2() - avar.getMinIndex2() + 1;
            subVars = new ArrayList<Variable>(rows * cols);
            for (int i = avar.getMinIndex(); i <= avar.getMaxIndex(); i++) {
                for (int j = avar.getMinIndex(); j <= avar.getMaxIndex(); j++) {
                    Variable sv = new Variable(avar.getType(), defaultBits);
                    bits += sv.getBits();
                    totalVars += sv.getTotalVars();
                    subVars.add(sv);
                }
            }
        }
    }

    public void visit(RemoplaStructVar svar) {
        int defaultBits = bits;
        bits = 0;
        totalVars = 0;
        Set<RemoplaVar> fields = svar.getType().getFields();
        subVars = new ArrayList<Variable>(fields.size());
        for (RemoplaVar f : fields) {
            Variable sv = new Variable(f, defaultBits);
            bits += sv.getBits();
            totalVars += sv.getTotalVars();
            subVars.add(sv);
        }
    }

    public void visit(RemoplaUnsupportedVar rvar) {
        bits = -1;
        subVars = null;
        totalVars = 0;
    }

    public void defaultCase(RemoplaVar obj) {
        logger.error("Default case reached in Variable.ConstructionManager.  This should never happen!");
        bits = -1;
        subVars = null;
        totalVars = 0;
    }

    // calculates the bits required to represent n
    // fast for small values of n (else a binary search would be quicker)
    // assumes n > 0;
    private int bitsRequired(int n) {
        int i = 0;
        while((n>>(++i)) > 0);
        return i;
    }
}
