/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// NullSemiring.java
// Author: Copied from jMoped2 (Dejvuth Suwimonteerabuth)
//
// A null semiring, copied from jMoped2 (i didn't want to use the jMoped package
// because it would be an odd reference to an otherwise unrelated package (and
// subject to change)).


package treeped.remoplatopds;

import java.util.Set;

import de.tum.in.wpds.Semiring;
import de.tum.in.wpds.CancelMonitor;

public class NullSemiring implements Semiring {

    public Semiring combine(Semiring a) {
        return this;
    }

    public Semiring extend(Semiring a, CancelMonitor monitor) {
        return this;
    }

    public Semiring extendDynamic(Semiring a, CancelMonitor monitor) {
        return this;
    }

    public Semiring extendPop(Semiring a, CancelMonitor monitor) {
        return this;
    }

    public Semiring extendPush(Semiring a, CancelMonitor monitor) {
        return this;
    }

    public void free() {
    }

    public Semiring id() {
        return this;
    }

    /**
     * {@inheritDoc}
     * @see Semiring#diff(Semiring)
     */
    public Semiring diff (Semiring arg0) {
        return null;
    }

    public Semiring lift(Semiring a) {
        return this;
    }

    public Semiring restrict(Semiring a) {
        return this;
    }

    public String toRawString() {
        return toString();
    }

    public String toString() {
        return "1";
    }

    public Set<Semiring> getGlobals() {
        return null;
    }

    public Semiring andWith(Semiring a) {
        return null;
    }

    public Semiring getEqClass() {
        return null;
    }

    public Semiring getEqRel(int flag) {
        return null;
    }

    public Semiring getGlobal() {
        return null;
    }

    public boolean isZero() {
        return false;
    }

    public Semiring orWith(Semiring a) {
        return null;
    }

    public void updateGlobal(Semiring a) {
    }

    public Semiring getEqClass(int flag) {
        return null;
    }

    public void sliceWith(Semiring eqclass, int approach) {
    }
}

