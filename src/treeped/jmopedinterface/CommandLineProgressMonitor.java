/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// CommandLineProgressMonitor.java
// Author: Matthew Hague
//
// A class to display the progress of underbone in a command line setting

package treeped.jmopedinterface;

import org.apache.log4j.Logger;

import de.tum.in.jmoped.underbone.ProgressMonitor;

public class CommandLineProgressMonitor implements ProgressMonitor {

    static Logger logger = Logger.getLogger(CommandLineProgressMonitor.class);

    private long timeStarted = 0;
    private boolean cancelled = false;
    private String name = "";

    public CommandLineProgressMonitor() {
    }

    /**
     * Tells the monitor to begin the task <code>name</code>.
     * <code>totalWork</code> determines the work to be done.
     *
     * @param name the task name.
     * @param totalWork the total work.
     */
    public void beginTask(String name, int totalwork) {
        logger.info("model checking task \"" + name + "\" started.");
        this.name = name;
        this.timeStarted = System.currentTimeMillis();
    }

    /**
     * Tells the monitor that the task is done.
     */
    public void done() {
        long timeEnded = System.currentTimeMillis();
        logger.info("Task completed in " + time(timeEnded - timeStarted));
    }

    /**
     * Tells the monitor to the amount specified by <code>work</code>
     * has been processed.
     *
     * @param work the amount that already processed.
     */
    public void worked(int work) {
    }



    /**
    * Sets cancel to <code>value</code>.
    *
    * @param value the cancel value.
    */
    public void setCanceled(boolean value) {
        cancelled = value;
    }

    /**
    * Returns <code>true</code> if canceled.
    *
    * @return <code>true</code> if canceled.
    */
    public boolean isCanceled() {
        return cancelled;
    }

    /**
    * Tells the monitor about the task <code>name</code>.
    *
    * @param name the task name.
    */
    public void subTask(String name) {
        logger.info("Task \"" + this.name + "\" has subtask \"" + name + "\"");
    }


    /////////////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    //

	private static String time(long t) {
		return t/1000 + "." + String.valueOf(1000+(t%1000)).substring(1);
	}

}


