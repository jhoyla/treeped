/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaMethodCall.java
// Author: Matthew Hague
//
// A method call.  This is neither an expression or a statement.  It may appear
// as a statement:
//      method(args)
// or as part of an assign:
//      i = method(args);
// The current implementation just stores the method name.  A reference to the
// method may be preferable, but clunky to implement.

package treeped.representation;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.StringBuffer;

public class RemoplaMethodCall {

    String callString;
    List<RemoplaExpr> parameters;

    /** Construct a method call
      *  @param callString the string to call the method with (non-null)
      *  @param parameters a list of parameter expressions (non-null)
      *  Currently it is assumed that the number of
      *  parameters is correct and that they are non-null.
      */
    public RemoplaMethodCall(String callString, List<RemoplaExpr> parameters) {
        assert callString != null && parameters != null;
        this.callString = callString;
        this.parameters = parameters;
    }

    /** Construct a method call
      *  @param callString the string to call the method with (non-null)
      *  @param parameters a list of parameter expressions, may be null if there
      *  are no parameters.  Currently it is assumed that the number of
      *  parameters is correct and that they are non-null.
      */
    public RemoplaMethodCall(String callString, RemoplaExpr... parameters) {
        assert callString != null;
        this.parameters = new ArrayList<RemoplaExpr>(parameters.length);
        for (int i = 0; i < parameters.length; i++) {
            this.parameters.add(parameters[i]);
        }
        this.callString = callString;
    }

    public String getCallString() {
        return callString;
    }

    public List<RemoplaExpr> getParameters() {
        return parameters;
    }

    public void setParameters(List<RemoplaExpr> parameters) {
        assert parameters != null;
        for (RemoplaExpr param : parameters)
            assert param != null;
        this.parameters = parameters;
    }

    /** Write the method call to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append(callString).append("(");

        if (parameters != null) {
            Iterator i = parameters.iterator();
            while(i.hasNext()) {
                RemoplaExpr arg = (RemoplaExpr)i.next();
                arg.toString(out);
                if(i.hasNext())
                    out.append(", ");
            }
        }

        out.append(")");

        return out;
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }



}
