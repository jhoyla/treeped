/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaArrayRefExpr.java
// Author: Matthew Hague
//
// A class for referencing array indexes in expressions

package treeped.representation;

import java.lang.StringBuffer;

public class RemoplaArrayRefExpr extends RemoplaExpr {
    private RemoplaArrayVar array;
    private RemoplaExpr index;
    private RemoplaExpr index2;

    public RemoplaArrayRefExpr(RemoplaArrayVar array, RemoplaExpr index) {
        assert array != null && index != null;
        this.array = array;
        this.index = index;
        this.index2 = null;
    }

    public RemoplaArrayRefExpr(RemoplaArrayVar array, RemoplaExpr index, RemoplaExpr index2) {
        assert array != null && index != null && index2 != null;
        this.array = array;
        this.index = index;
        this.index2 = index2;
    }

    /** @return the number of dimensions to the array access **/
    public int getDimension() {
        return index2 == null ? 1 : 2;
    }

    public RemoplaExpr getIndex() {
        return index;
    }

    public RemoplaExpr getIndex2() {
        return index2;
    }

    public RemoplaArrayVar getArray() {
        return array;
    }

    /** Writes array reference to string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        array.toString(out).append("[");
        return index.toString(out).append("]");
    }

    public void accept(RemoplaExprVisitor visitor) {
        visitor.visit(this);
    }

}
