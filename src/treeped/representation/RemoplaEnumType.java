/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaEnumType.java
// Author: Matthew Hague
//
// A class for declared enum types



package treeped.representation;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import java.lang.StringBuffer;

import org.apache.log4j.Logger;

public class RemoplaEnumType extends RemoplaDeclaredType {

    static Logger logger = Logger.getLogger(RemoplaEnumType.class);

    private static final String ENUM = "enum";

    private Set<RemoplaEnumConstant> values;

    public RemoplaEnumType(String name) {
        super(name);
        this.values = new HashSet<RemoplaEnumConstant>();
    }

    public RemoplaEnumType(String name, Set<RemoplaEnumConstant> values) {
        super(name);
        for (RemoplaEnumConstant val : values) {
            assert val != null;
        }

        this.values = values;
    }


    /** Adds a value to the values the enum can take, silent if duplicate
     *   @param value the RemoplaEnumConstant of the value, non-null
     */
    public void addValue(RemoplaEnumConstant value) {
        assert value != null;
        values.add(value);
    }

    public Set<RemoplaEnumConstant> getValues() {
        return values;
    }


    /** Writes the type declaration to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append(ENUM).append(" ").append(this.getName()).append(" { ");
        Iterator i = values.iterator();
        while (i.hasNext()) {
            RemoplaEnumConstant val = (RemoplaEnumConstant)i.next();
            val.toString(out);
            if (i.hasNext())
                out.append(", ");
        }
        return out.append(" };");
    }


    /** Writes the Remopla declaration prefix string for a variable of the type
     * to the passed buffer
     *   @param out the string buffer to write to
     *   @return the string buffer
     */
    public StringBuffer getDeclarationPrefix(StringBuffer out) {
        return out.append(ENUM).append(" ").append(this.getName());
    }
}
