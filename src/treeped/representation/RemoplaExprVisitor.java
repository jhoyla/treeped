/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaExprVisitor.java
// Author: Matthew Hague
//
// An interface for a visitor to RemoplaExprs

package treeped.representation;

public interface RemoplaExprVisitor {
    public void visit(RemoplaArrayRefExpr obj);
    public void visit(RemoplaArrayVar obj);
    public void visit(RemoplaBinExpr obj);
    public void visit(RemoplaBoolConstant obj);
    public void visit(RemoplaBoolVar obj);
    public void visit(RemoplaEnumConstant obj);
    public void visit(RemoplaEnumVar obj);
    public void visit(RemoplaIntConstant obj);
    public void visit(RemoplaIntVar obj);
    public void visit(RemoplaStructVar obj);
    public void visit(RemoplaUnaryExpr obj);
    public void visit(RemoplaUndefExpr obj);
    public void visit(RemoplaUnsupportedVar obj);
    public void visit(RemoplaVoidConstant obj);
    public void visit(RemoplaIntConstVar obj);
}
