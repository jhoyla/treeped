/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaDeclaredType.java
// Author: Matthew Hague
//
// A class that gives an abstract handling of user defined types
// Intended sub-classes -- RemoplaEnumType and RemoplaStructType


package treeped.representation;

import java.lang.StringBuffer;


public abstract class RemoplaDeclaredType {
    private String name;

    public RemoplaDeclaredType(String name) {
        assert name != null;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    /** Writes the type declaration to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public abstract StringBuffer toString(StringBuffer out);

    /** @return the Remopla declaration string that should precede the variable
     * name
     */
    public String getDeclarationPrefix() {
        return getDeclarationPrefix(new StringBuffer()).toString();
    }

    /** @return the Remopla declaration string that should follow the variable
     * name
     */
    public String getDeclarationSuffix() {
        return getDeclarationSuffix(new StringBuffer()).toString();
    }


    /** Writes the Remopla declaration prefix string for a variable of the type
     * to the passed buffer
     *   @param out the string buffer to write to
     *   @return the string buffer
     */
    public abstract StringBuffer getDeclarationPrefix(StringBuffer out);


    /** Writes the Remopla declaration suffix string for a variable of the type
     * to the passed buffer
     *   @param out the string buffer to write to
     *   @return the string buffer
     */
    public StringBuffer getDeclarationSuffix(StringBuffer out) {
        return out;
    };

}
