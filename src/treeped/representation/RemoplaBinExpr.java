/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaBinExpr.java
// Author: Matthew Hague
//
// Expressions of the form a op b.

package treeped.representation;

import java.lang.StringBuffer;

public class RemoplaBinExpr extends RemoplaExpr {
    static public enum Op {
        ADD("+"), SUB("-"), DIV("/"), MUL("*"), AND("&&"),
        OR("||"), XOR("^^"), EQ("=="), NEQ("!="), LT("<"),
        LTE("<="), GT(">"), GTE(">="), SHL("<<"), SHR(">>");

        private final String sym;

        Op(String sym) { this.sym = sym; }
    }

    RemoplaExpr left;
    Op type;
    RemoplaExpr right;

    /** Constructs a binary expression left op right
      *  @param left left hand side expression, non-null
      *  @param type the kind of bin op: RemoplaBinOp.ADD, SUB, MUL, AND, OR, XOR
      *  EQ, NEQ, LT, LTE, GT, GTE
      *  @param right the right hand side expression, non-null
      */
    public RemoplaBinExpr(RemoplaExpr left, Op type, RemoplaExpr right) {
        assert left != null && right != null;
        this.left = left;
        this.right = right;
        this.type = type;
    }

    public RemoplaExpr getLeft() {
        return left;
    }

    public RemoplaExpr getRight() {
        return right;
    }

    public Op getType() {
        return type;
    }

    /** Writes the expression to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append("(");
        left.toString(out);
        out.append(" " + type.sym + " ");
        right.toString(out);
        out.append(")");
        return out;
    }


    public void accept(RemoplaExprVisitor visitor) {
        visitor.visit(this);
    }
}
