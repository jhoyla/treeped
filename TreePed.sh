#!/bin/sh

wd=$(cd "$(dirname "$0")"; pwd)
echo $wd

cp=$CLASSPATH

for file in $(find lib/ -name '*.jar')
do
    cp=${cp}:${file}
done

cp=${cp}:${wd}/build/classes


java -ea -cp "$cp" -Xmx2G treeped.main.Main "$@"
